digit 是位數的意思，個位數、十位數。

- Given an array of integers and a value, determine if there are any two integers in the array which sum equal to the given value.

https://www.educative.io/collection/page/5642554087309312/5679846214598656/830001


In this solution, we'll use the following algorithm to find a pair that sum up to target (say 'val')