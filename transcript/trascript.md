# two sum

in this solution, we'll use the following algorithm to find a pair that sum up to the target value.

Steps:
    
    - scan the whole array once and store visited elements/items into a hash set
    - During scan, for every element in this arrya, we will check if `target val` - 'e' is present in the hash set

    + if the value is found in the hash set, it means there's a pair in that array whose sum is equal to the given val

    - if we have exhausted all elements in the array and didn't find any such pair. our funciton will return false.

# reverse a singly linked list

題目描述

given a pointer/reference to the head of a singly linked list.


return the pointer to the head of the reversed singly linked list as shown in the figure.

How would you do it iteratively in a single pass.

How would you do it recursively in a single pass



# Reverse k Elements

Given a singly linked list and an integer 'k', reverse every 'k' elements. If k <= 1, then input list is unchanged. If k >= n (n is the length of linked list), then reverse the whole linked list.

Below is the input linked list and output after reversing every 3 elements.


## solution


We'll work upon a sub-list of size 'k' at a time

Once that sub-list is reversed, we have its head and tail in current_head and current_tail __respectively__.

If it was the first sub-list of size k.

its head is the head of the output reversed list.
We'll point the `reversed` to current_head of the first sublist.

If it were the 2nd or following sublist, we'll connect the tail of the previous sublist into the head of the current sublist.

Let's apply this algorithm on the following list with 7 elements and k = 5



As the reversed pointer is null after the first sublist is reversed, it'll be updated with the current_head. And it wil be the head of the output list.
prev_tail will be updated with the current_tail


Then we'll reverse the next sublist of size 2 and update current_head and current_tail accordingly. head will become null as there will be no remaining list.
