"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isDone(nums) {
    var result = true;
    nums.map(function (element) {
        if (Array.isArray(element)) {
            result = false;
        }
    });
    return result;
}
function swap(nums, from, to) {
    var temp = nums[from];
    nums[from] = nums[to];
    nums[to] = temp;
    return nums;
}
function cleanRtnValue(data) {
    if (Array.isArray(data) && data.length === 1) {
        return data[0];
    }
    else {
        return data;
    }
}
function doSort(nums, outputQue) {
    if (nums.length === 1) {
        outputQue.push(nums[0]);
        return outputQue;
    }
    var leftBound = 0;
    var lastIndex = nums.length - 1;
    var pivot = nums[lastIndex];
    Array(lastIndex).fill(null).map(function (_, i) {
        if (nums[i] < pivot) {
            var tmp = nums[leftBound];
            swap(nums, leftBound, i);
            leftBound += 1;
            console.log(nums);
            console.log(leftBound);
        }
        else {
            console.log("no swap " + nums);
        }
    });
    swap(nums, leftBound, lastIndex);
    outputQue.push(cleanRtnValue(nums.slice(0, leftBound)));
    if (leftBound === lastIndex) {
        outputQue.push(nums[lastIndex]);
    }
    else {
        outputQue.push(nums[leftBound]);
        outputQue.push(cleanRtnValue(nums.slice(leftBound + 1, lastIndex + 1)));
    }
    return outputQue;
}
function quick_sort(nums) {
    var queue = [nums];
    var _loop_1 = function () {
        var tmpQueue = [];
        queue.map(function (ele) {
            if (Array.isArray(ele) && ele.length > 0) {
                doSort(ele, tmpQueue);
            }
            else {
                tmpQueue.push(ele);
            }
        });
        queue = tmpQueue;
    };
    while (false == isDone(queue)) {
        _loop_1();
    }
    return queue;
}
exports.quick_sort = quick_sort;
quick_sort([9, 1, 2, 5, 6]);
