// Given nums = [2, 7, 11, 15], target = 9,
//
//     Because nums[0] + nums[1] = 2 + 7 = 9,
// return [0, 1].

export default class TwoSum {

    constructor() {
    }

    do(nums, target){
        for (let i = 0; i <= nums.length-2; i++) {
            for(let j=i+1;j <= nums.length-1; j ++){
                if(nums[i]+nums[j] === target){
                    return [i, j];
                }
            }
        }
    }
}

