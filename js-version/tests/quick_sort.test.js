import {quick_sort} from "../quick_sort";
describe('Quick sort', () => {
   it('should be a valid format', () => {
          // expect(quick_sort([9, 1, 2, 5, 6])).toEqual([1, 2, 5, 6, 9])
          // expect(quick_sort([9, 2, 7, 5, 8, 1, 4, 3, 6])).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9])
          expect(
              quick_sort([9, 2, 7, 5, 8, 1, 5, 5, 4, 3, 6])
          ).toEqual(
              [1, 2, 3, 4, 5, 5, 5, 6, 7, 8, 9]
          )
       }
   )
})
