// Given nums = [2, 7, 11, 15], target = 9,
//
//     Because nums[0] + nums[1] = 2 + 7 = 9,
// return [0, 1].
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TwoSum = (function () {
    function TwoSum() {
    }
    TwoSum.prototype.do = function (nums, target) {
        for (var i = 0; i <= nums.length - 2; i++) {
            for (var j = i + 1; j <= nums.length - 1; j++) {
                if (nums[i] + nums[j] === target) {
                    return [i, j];
                }
            }
        }
    };
    return TwoSum;
}());
exports.default = TwoSum;
