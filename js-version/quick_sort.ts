function isDone(nums): Boolean {
    let result = true
    nums.map(element => {
        if (Array.isArray(element)) {
            result = false
        }
    })
    return result
}
function swap(nums: Array<number>, from: number, to: number){
    let temp = nums[from]
    nums[from] = nums[to]
    nums[to] = temp
    return nums
}

function cleanRtnValue(data) {
    if (Array.isArray(data) && data.length === 1) {
        return data[0]
    } else {
        return data
    }
}

function doSort(nums: Array<number>, outputQue: Array<any>) {
    if(nums.length===1){
        outputQue.push(nums[0])
        return outputQue
    }
    let leftBound = 0;
    let lastIndex = nums.length - 1;
    let pivot = nums[lastIndex]
    Array(lastIndex).fill(null).map((_, i) => {
            if (nums[i] < pivot) {
                let tmp = nums[leftBound]
                swap(nums, leftBound, i)
                leftBound += 1
                console.log(nums)
                console.log(leftBound)
            }else{
                console.log(`no swap ${nums}`)
            }
        }
    );
    swap(nums, leftBound, lastIndex);
    outputQue.push(cleanRtnValue(nums.slice(0,leftBound)));
    if(leftBound===lastIndex){
        outputQue.push(nums[lastIndex]);
    }else{
        outputQue.push(nums[leftBound]);
        outputQue.push(cleanRtnValue(nums.slice(leftBound+1, lastIndex+1)));
    }
    return outputQue
}

export function quick_sort(nums: Array<number>) {
    let queue = [nums]
    while (false == isDone(queue)) {
        let tmpQueue = []
        queue.map(ele => {
            if (Array.isArray(ele) && ele.length > 0) {
                doSort(ele, tmpQueue)
            } else {
                tmpQueue.push(ele)
            }
        })
        queue = tmpQueue
    }
    return queue
}

quick_sort([9, 1, 2, 5, 6]);