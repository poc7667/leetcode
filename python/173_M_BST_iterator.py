import unittest
import pprint
import pdb
import time
import Queue
from BinarySearchTree.treenode import TreeNode
pp = pprint.PrettyPrinter(indent=4)


# Definition for a  binary tree node
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class BSTIterator(object):
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.sorted_nodes =[]
        self.build_nodes(root)
        self.sorted_nodes = self.sorted_nodes[::-1]

    def build_nodes(self, node):
        if node == None:
            return False
        elif node.left == None and node.right == None:
            self.sorted_nodes.append(node)
        else:
            self.build_nodes(node.left)
            self.sorted_nodes.append(node)
            self.build_nodes(node.right)


    def hasNext(self):
        """
        :rtype: bool
        """
        if len(self.sorted_nodes)!=0:
            return True
        return False

    def next(self):
        """
        :rtype: int
        """
        current_node = self.sorted_nodes.pop()
        return current_node.val


# Your BSTIterator will be called like this:
# i, v = BSTIterator(root), []
# while i.hasNext(): v.append(i.next())

class MyTestCase(unittest.TestCase):
    def test_something(self):

        # treenode = TreeNode(1, 2, 3)
        # print treenode
        # serialized_data = treenode.dump()
        # treenode.load(serialized_data)
        # self.assertEqual(treenode.entry.key, 1)
        # self.assertEqual(treenode.left_node_address, 2)
        # self.assertEqual(treenode.right_node_address, 3)
        # print serialized_data
        # sol = Solution()
        # self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
