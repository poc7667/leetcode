import unittest
import pdb
import Queue


class Solution(object):

    def do_cal(self, val, index, curr_val):
        if index > len(val) - 1:
            val.append(0)
        new_val = val[index] + curr_val
        if new_val < 10:
            val[index] = new_val
        else:
            val[index] = new_val % 10
            self.do_cal(val, index + 1, new_val / 10)

    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        num1_va1 = []  # max 109
        num2_va1 = []  # max 109
        val = []  # max 109+109-1
        if len(num1) == 1 and num1[0] == '0':
            return '0'
        if len(num2) == 1 and num2[0] == '0':
            return '0'

        for i in num1:
            num1_va1.append(i)
        for i in num2:
            num2_va1.append(i)
        for i in range(0, len(num2_va1)):
            for j in range(0, len(num1_va1)):
                self.do_cal(val, i + j, int(num2_va1[len(num2_va1) - i - 1]) * int(num1_va1[len(num1_va1) - j - 1]))
        result = ''
        for i in reversed(val):
            result += str(i)
        return result


class MyTestCase(unittest.TestCase):
    def test_something(self):
        inst = Solution()
        self.assertEqual('1068', inst.multiply('12', '89'))

    # Given word = "SEE", return true.
    # Given word = "ABCB", return false.


if __name__ == '__main__':
    unittest.main()
