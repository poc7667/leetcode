import unittest
import pdb
import Queue


class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False
        q = Queue.Queue()
        x_str = str(x)
        for i in range(0, len(x_str)/2):
            q.put(x_str[i])
        for i in range(0, len(x_str)/2):
            pop_val = q.get()
            if pop_val != x_str[len(x_str)-i-1]:
                return False
        return True



class MyTestCase(unittest.TestCase):

    def test_something(self):
        inst = Solution()
        self.assertEqual(True, inst.isPalindrome(121))
        self.assertEqual(False, inst.isPalindrome(-121))

    # Given word = "SEE", return true.
    # Given word = "ABCB", return false.


if __name__ == '__main__':
    unittest.main()
