# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
from memory_profiler import profile

"""
236. Lowest Common Ancestor of a Binary Tree
Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
According to the definition of LCA on Wikipedia: "The lowest common ancestor is defined between two nodes v and w as the lowest node in T that has both v and w as descendants (where we allow a node to be a descendant of itself)."
        _______3______
       /              \
    ___5__          ___1__
   /      \        /      \
   6      _2       0       8
         /  \
         7   4
For example, the lowest common ancestor (LCA) of nodes 5 and 1 is 3. Another example is LCA of nodes 5 and 4 is 5, since a node can be a descendant of itself according to the LCA definition.
"""

"""
Recursion lca(node) =
"""
import time
import tree_util
import tree_helper

p = print


# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    def __repr__(self):
        if self.val != None and self.left and self.right:
            return "({left})-{value}-({right})".format(value=self.val, left=self.val.left.val, right=self.val.right.val)
        elif self.val != None and self.left :
            return "({left})-{value}".format(value=self.val, left=self.val.left.val)
        elif self.val != None and self.left and self.right:
            return "({left})-{value}-({right})".format(value=self.val, left=self.val.left.val, right=self.val.right.val)

def find_by_value(orig_root, v):
    root = orig_root
    Q = [root]
    if root:
        while True:
            next_q = []
            if len(Q)==0:
                return False
            for node in Q:
                if node==None: continue
                if node.val == v:
                    return node
                else:
                    next_q.append(node.left)
                    next_q.append(node.right)
            Q = next_q
    else:
        return False

def dfs_search_with_track(root, v):
    Q = [root]
    visited_left = set()
    visited_right = set()
    while Q:
        u = Q[-1]
        if u == None:
            Q.pop()
            continue
        if u == v:
            break
        if u not in visited_left:
            visited_left.add(u)
            Q.append(u.left)
        elif u not in visited_right:
            visited_right.add(u)
            Q.append(u.right)
        else:
            Q.pop()
    return Q

class Solution(object):
    def lowestCommonAncestor(self, root, p, q):

        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        print(p, q)
        if p and q:
            p_track = dfs_search_with_track(root, p)
            q_track = dfs_search_with_track(root, q)
            lca = None
            for p_node in p_track[::-1]:
                if p_node in q_track:
                    lca = p_node
                    break
            return lca
        else:
            return None

    def find_lca(self, p, q, pre, ino):
        while len(pre) > 0:
            root = pre[0]
            print("root", root)
            if root == p: return p
            if root == q: return q
            left_part_len = ino.index(root)
            if ino.index(root) > ino.index(p) and ino.index(root) > ino.index(q):
                pre = pre[1:left_part_len + 1]
                ino = ino[:left_part_len]
                continue
            elif ino.index(root) < ino.index(p) and ino.index(root) < ino.index(q):
                right_part_start_position = 1 + left_part_len
                pre = pre[right_part_start_position:]
                ino = ino[right_part_start_position:]
                continue
            else:
                return root

    def get_inorder_seq(self, root):
        output = []
        if root:
            visited_left = set()
            visited_right = set()
            Q = [root]
            while len(Q):
                i = Q.pop()
                if i:
                    Q.append(i)
                    if i not in visited_left:
                        Q.append(i.left)
                        visited_left.add(i)
                    elif i not in visited_right:
                        Q.pop()
                        Q.append(i.right)
                        output.append(i)
                        visited_right.add(i)
        return output

    def get_preorder_seq(self, root):
        output = []
        if root:
            visited_left = set()
            visited_right = set()
            Q = [root]
            while Q:
                u = Q[-1]
                if u == None:
                    Q.pop()
                    continue
                if u not in visited_left and u not in visited_right:
                    output.append(u)
                    visited_left.add(u)
                    Q.append(u.left)
                elif u in visited_left and u not in visited_right:
                    visited_right.add(u)
                    Q.append(u.right)
                else:
                    Q.pop()
        return output



class TestCase(unittest.TestCase):
    def setUp(self):
        import test_data
        self.s = Solution()
        self.tree = tree_helper.Tree()
        self.fixture = [-64, 12, 18, -4, -53, None, 76, None, -51, None, None, -93, 3, None, -31, 47, None, 3, 53, -81,
                        33, 4,
                        None,
                        -51, -44, -60, 11, None, None, None, None, 78, None, -35, -64, 26, -81, -31, 27, 60, 74, None,
                        None, 8,
                        -38,
                        47, 12, -24, None, -59, -49, -11, -51, 67, None, None, None, None, None, None, None, -67, None,
                        -37, -19,
                        10,
                        -55, 72, None, None, None, -70, 17, -4, None, None, None, None, None, None, None, 3, 80, 44,
                        -88, -91,
                        None,
                        48, -90, -30, None, None, 90, -34, 37, None, None, 73, -38, -31, -85, -31, -96, None, None, -18,
                        67, 34,
                        72,
                        None, -17, -77, None, 56, -65, -88, -53, None, None, None, -33, 86, None, 81, -42, None, None,
                        98, -40, 70,
                        -26, 24, None, None, None, None, 92, 72, -27, None, None, None, None, None, None, -67, None,
                        None, None,
                        None,
                        None, None, None, -54, -66, -36, None, -72, None, None, 43, None, None, None, -92, -1, -98,
                        None, None,
                        None,
                        None, None, None, None, 39, -84, None, None, None, None, None, None, None, None, None, None,
                        None, None,
                        None,
                        -93, None, None, None, 98]
        self.fixture2 = test_data.longest_test_data
        self.fixture3 = [37,-34,-48,None,-100,-100,48,None,None,None,None,-54,None,-71,-22,None,None,None,8]



    # def test_inorder(self):
    #     tree = tree_helper.Tree()
    #     tree.load_array([1, None, 2, 3])
    #     root = tree.head
    #     self.assertEqual([root, root.right.left, root.right], self.s.get_inorder_seq(root))
    #
    # def test_preorder(self):
    #     tree = tree_helper.Tree()
    #     tree.load_array([1, None, 2, 3])
    #     root = tree.head
    #     self.assertEqual([root, root.right, root.right.left], self.s.get_preorder_seq(root))
    #
    #
    # def test_default2(self):
    #
    # def test_default(self):
    #     s = Solution()
    #     self.tree.load_array(self.fixture2)
    #     root = self.tree.head
    #     self.assertEqual(
    #         s.lowestCommonAncestor(root,
    #                                find_by_value(root, 9998),
    #                                find_by_value(root, 9999)),
    #         12)

    def test_dflt3(self):
        s = Solution()
        self.tree.load_array(self.fixture3)
        root = self.tree.head
        self.assertEqual(
            s.lowestCommonAncestor(root,
                                   find_by_value(root, -100),
                                   find_by_value(root, -71)),
            find_by_value(root, -48))

    #     #


if __name__ == "__main__":
    unittest.main()
