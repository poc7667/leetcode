# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        # is max?
        finished = False
        print(nums)

        def get_target(nums, i):
            swap_val = nums[i-1]
            j = i+1
            while j < len(nums):
                if nums[j] <= swap_val:
                    return j-1
                j+=1
            return len(nums)-1


        if len(nums) > 1 and not finished:
            found =False
            i = 0
            while True:
                if nums[i] < nums[i + 1]:
                    break
                if i == len(nums) - 2:
                    nums.sort()
                    found = True
                    break
                i += 1

            if not found:
                i = len(nums) - 1
                while True:
                    if nums[i - 1] < nums[i]:
                        target = get_target(nums, i)
                        nums[i -1], nums[target] = nums[target], nums[i-1]
                        nums[i:] = sorted(nums[i:])
                        break
                    if i == 1:
                        break
                    i -= 1

class MyTestCase(unittest.TestCase):

    def test_something(self):
        sol = Solution()
        num = [1,3,2]
        sol.nextPermutation(num)
        self.assertEqual([2,1,3], num)

        num = [1,2,3]
        sol.nextPermutation(num)
        self.assertEqual([1,3,2], num)



        num = [2,3,1]
        sol.nextPermutation(num)
        self.assertEqual([3,1,2], num)


        num = [3,2,1]
        sol.nextPermutation(num)
        self.assertEqual([1,2,3], num)

        num = [1,5,1]
        sol.nextPermutation(num)
        self.assertEqual([5,1,1], num)

        # self.assertEqual(None, sol.nextPermutation([2,3,1]))
