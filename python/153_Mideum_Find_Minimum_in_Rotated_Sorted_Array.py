# Suppose a sorted array is rotated at some pivot unknown to you beforehand.
#
# (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
#
# Find the minimum element.
#
# You may assume no duplicate exists in the array.
#
# Subscribe to see which companies asked this question
import unittest
import traceback

import pdb
NOT_FOUND = -1


class Solution(object):
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums)==1:
            return nums[0]
        if len(nums) < 1:
            return NOT_FOUND
        start = 0
        end = len(nums) - 1
        while start < end:
            mid = start + (end - start) / 2
            print "{nums} {s}/{m}/{e}".format(nums=nums, s=start, m=mid, e=end)

            if self.isOnlyTwoElementsLeft(start, end):
                if nums[start] < nums[end]:
                    return nums[start]
                else:
                    return nums[end]
            if self.isWholeArrayAlreadySorted(nums, start, mid, end):
                return nums[start]
            elif self.isMidOnLeftPart(nums, start, mid, end):
                if self.isWholeArrayAlreadySorted(nums, mid, mid, end):
                    return nums[mid]
                else:
                    start = mid
            elif self.isMidOnRightPart(nums, start, mid, end):
                if self.isWholeArrayAlreadySorted(nums, start, mid, mid):
                    return nums[start]
                else:
                    end = mid

            if self.isOnlyTwoElementsLeft(start, end):
                if nums[start] < nums[end]:
                    return nums[start]
                else:
                    return nums[end]
        return NOT_FOUND

    def isWholeArrayAlreadySorted(self, nums, start, mid, end):
        print "test {start} {end} {range} mid={mid}".format(
            start=nums[start], end=nums[end], range=nums[start:end+1], mid=nums[mid])
        if nums[start] <= nums[mid] <= nums[end]:
            return True
        else:
            return False

    def isOnlyTwoElementsLeft(self, start, end):
        if end-start == 1:
            return True
        else:
            return False

    def isMidOnLeftPart(self, nums, start, mid, end):
        if nums[end] < nums[start] < nums[mid]:
            return True
        else:
            return False

    def isMidOnRightPart(self, nums, start, mid, end):
        if nums[mid] < nums[end] < nums[start]:
            return True
        else:
            return False


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        self.assertEqual(app.findMin([1, 2, 3, 4, 5]), 1)
        self.assertEqual(app.findMin([3, 4, 5, 1, 2]), 1)
        self.assertEqual(app.findMin([3, 4, 5, 2]), 2)
        self.assertEqual(app.findMin([5, 1]), 1)
        self.assertEqual(app.findMin([3, 1, 2]), 1)
        self.assertEqual(app.findMin([2]), 2)


if __name__ == "__main__":
    unittest.main()
