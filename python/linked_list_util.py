# -*- coding: utf8 -*-
from __future__ import print_function
import pdb
import unittest

DUMMY_VALUE = -9999


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")

def create_a_linked_list_from_list(val_lst):
    if len(val_lst) > 0:
        head = curr = ListNode(val_lst[0])
        for val in val_lst[1:]:
            curr.next = ListNode(val)
            curr = curr.next
        return head

def convert_linked_list_into_a_list(head):
    value_lst = []
    while head != None:
        value_lst.append(head.val)
        head = head.next
    return value_lst

def get_last_node(head):
    while head.next != None:
        head = head.next
    return head


def get_mid_node(start, termination_node=None):
    fast = start
    slow = start
    while fast != termination_node:
        fast = fast.next
        if fast == termination_node:
            break
        if fast.next != termination_node:
            slow = slow.next
            fast = fast.next
        else:
            break
    return slow

# def remove(start, n1):
#     dummy_node = ListNode(DUMMY_VALUE)
#     dummy_node.next = start

def move(start, node, prepended):
    dummy_node = ListNode(DUMMY_VALUE)
    dummy_node.next = start


def swap_nodes(start, n1, n2):
    dummy_node = ListNode(DUMMY_VALUE)
    dummy_node.next = start
    p1 = prev_node(dummy_node, n1)
    p2 = prev_node(dummy_node, n2)
    if n1.next == n2:  # adjacent
        p1.next = n2
        n1.next = n2.next
        n2.next = n1
    else:  # normal swap
        p1.next, p2.next = p2.next, p1.next
        n1.next, n2.next = n2.next, n1.next
    return dummy_node.next


def ls(start):
    node_values = []
    while start != None:
        node_values.append(start.val)
        print("{value}".format(value=start.val), end=" ")
        start = start.next
    print("\n")
    return node_values

def print_node(start, end):
    try:
        curr = start
        while curr!=end:
            print(curr, end=", ")
            curr = curr.next
        print("\n")
    except:
        pdb.set_trace()
        pass

def prev_node(start, n1):
    dummy_node = ListNode(DUMMY_VALUE)
    dummy_node.next = start
    while dummy_node.next != n1:
        dummy_node = dummy_node.next
    return dummy_node



class TestCase(unittest.TestCase):
    def test_default(self):
        None


if __name__ == "__main__":
    unittest.main()
