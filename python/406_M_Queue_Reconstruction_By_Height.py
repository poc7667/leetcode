# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# Input:
# [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
#
# Output:
# [[5,0], [7,0], [5,2], [6,1], [4,4], [7,1]]

class Solution(object):
    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        """
        output = []
        nums = []

        plp = sorted(people, key=lambda x: (-x[0], x[1]))
        for i,val in enumerate(plp):
            output.insert(val[1], val)
        return output

        # while len(output) < len(people):
        #     highest = plp.pop()
        #     if len(output)==0:
        #         output.append(highest)
        #         continue
        #
        #     for i in range(len(output)):
        #         if highest[0] == output[i][0]:
        #             if highest[1] < output[i][1]:
        #                 output.append(i, highest)
        #         else:
        #

        # None

        # while len(output) < len(people):
        #     for plp in people:
        #         cnt = 0
        #         for i in range(len(output)):
        #             placeholder = output[i]
        #             if placeholder[0] > plp[0] and plp[1] < placeholder[1]:
        #                 #
        #             if placeholder[0] >= plp[0]:
        #                 cnt+=1
        #             if plp[1] >= cnt:
        #                 output.insert(cnt, plp)
        #     print(plp)

        # for i in sorted(nums):
        #     current_group = sorted(sortedPlp[i], key=lambda x: x[0])
        #     for j in range(len(current_group)):
        #         curren_node = current_group[j]
        #         if len(output)==0:
        #             output.append(curren_node)
        #         else:
        #             count=0
        #             k_idx = 0
        #             for k in output:
        #                 if k[0]>=curren_node[0]:
        #                     count+=1
        #                 if count==k[1]:
        #                     output.insert(k_idx, curren_node)
        #                 k_idx+=1
        # return output

        # sortedPlp = sorted(people, key= lambda x: x[1])


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, sol.reconstructQueue([[7, 0], [4, 4], [7, 1], [5, 0], [6, 1], [5, 2]]))
