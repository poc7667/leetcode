import unittest
import pdb


class Solution(object):
    def subsetsWithDup(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        nums=sorted(nums)
        last_round = []
        results = [[]]
        for i, val in enumerate(nums):
            if i > 0 and nums[i]==nums[i-1]:
                _last_round = last_round[:]
                last_round = []
                for j in _last_round:
                    new_set = j+[val]
                    last_round.append(new_set)
                    results.append(new_set)
            else:
                last_round = []
                for j in range(len(results)):
                    current_set = results[j]
                    new_set = current_set+[val]
                    last_round.append(new_set)
                    results.append(new_set)
        return results

class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual([[],[2],[2,2],[1],[1,2],[1,2,2]], sol.subsetsWithDup([1,2,2]))

# if __name__ == '__main__':
#    unittest.main()
