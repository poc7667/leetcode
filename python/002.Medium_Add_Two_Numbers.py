# -*- coding: utf8 -*-

import pdb
import unittest


# You are given two linked lists representing two non-negative numbers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
#
# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)

# Output: 7 -> 0 -> 8

# Definition for singly-linked list.



class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        num1 = self.to_value(l1)
        num2 = self.to_value(l2)
        return self.value_to_link_list(num1 + num2)

    def to_value(self, head):
        digits = []
        if head == None:
            return [0]
        while head != None:
            digits.append(head.val)
            head = head.next
        return self.digits_to_value(digits)

    def digits_to_value(self, digits):
        value = 0
        base = 1
        for i in digits:
            value += base * i
            base *= 10
        return value

    def value_to_link_list(self, value):
        print(value)
        digits = []
        current_nod = ListNode(0)
        last_nod = None
        while value > 0:
            digits.append(value % 10)
            value /= 10
        digits.reverse()

        for i in range(len(digits)):
            if last_nod:
                current_nod = ListNode(digits[i])
                current_nod.next = last_nod
                last_nod = current_nod
            else:
                last_nod = current_nod = ListNode(digits[i])
        return current_nod

def create_link_list(nums):
    list = []
    for i in nums:
        list.append(ListNode(i))
    for i in range(len(list) - 1):
        list[i].next = list[i + 1]
    return list[0]

class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        l1 = create_link_list([2, 4, 3])
        l2 = create_link_list([5, 6, 4])
        l3 = create_link_list([7, 0, 8])
        output = Solution().addTwoNumbers(l1,l2)
        self.assertEqual(app.to_value(output), app.to_value(l3))

    def test_2(self):
        app = Solution()
        l1 = create_link_list([1,8])
        l2 = create_link_list([0])
        l3 = create_link_list([1,8])
        output = Solution().addTwoNumbers(l1, l2)
        self.assertEqual(app.to_value(output), app.to_value(l3))

if __name__ == "__main__":
    unittest.main()
