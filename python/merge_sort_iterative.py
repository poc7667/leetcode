# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print

def chunks(nums, size):
    for i in range(0, len(nums), 2*size):
        yield (i, i+size)

class Solution(object):
    def iterative_sort(self, nums):
        size = 1
        while size <= len(nums):
            print("before merge", nums, "size", size)
            for first, second in chunks(nums, size):
                self.merge(nums, first, second, size)
                print(nums)
            size *= 2
        return nums

    def merge(self, nums, first, second, chunck_size):
        Q = []
        first_end = first + chunck_size
        if second + chunck_size >= len(nums):
            second_end = len(nums)
        else:
            second_end = second + chunck_size
        L = nums[first: first_end]
        R = nums[second: second_end]
        while len(L) and len(R):
            if L[0] <= R[0]:
                Q.append(L[0])
                del L[0]
            else:
                Q.append(R[0])
                del R[0]
        if not len(L): Q.extend(R)
        if not len(R): Q.extend(L)
        for i in range(len(Q)):
            nums[i+first] = Q[i]

class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = Solution()

    # def test_merge(self):
    #     self.assertEqual([1, 3, 5, 9],
    #                      self.app.merge([1, 9], [3, 5]))
    #
    #     self.assertEqual([1, 3, 5, 9],
    #                      self.app.merge([1, 9], [3, 5]))
    #
    #     self.assertEqual([1, 9],
    #                      self.app.merge([1, 9], []))

    def test_sort(self):
        # self.assertEqual([1, 2, 3, 4, 5, 6],
        #                  self.app.iterative_sort([1, 6, 2, 3, 5, 4]))

        self.assertEqual([3,5,7],
                         self.app.iterative_sort([7,5,3]))

        self.assertEqual([1, 3, 4, 5, 6],
                         self.app.iterative_sort([1, 6, 3, 5, 4]))


if __name__ == "__main__":
    unittest.main()
