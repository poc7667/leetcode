# -*- coding: utf8 -*-

# Rotate an array of n elements to the right by k steps.
#
# For example, with n = 7 and k = 3,
# the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
#
# Note:
# Try to come up as many solutions as you can,
# there are at least 3 different ways to solve this problem.
#
# [show hint]
#
# Hint:
# Could you do it in-place with O(1) extra space?
# Related problem: Reverse Words in a String II
import unittest
import pdb


class Solution(object):
    def rotate(self, nums, k):
        arr_size = len(nums)
        k = k % arr_size
        moving_len = arr_size - k
        remaining_moving_len = arr_size - moving_len

        for i in range(moving_len/2):
            start = i
            end = (moving_len - 1) - i
            nums[start], nums[end] = nums[end], nums[start]

        for i in range(remaining_moving_len/2):
            start = i + moving_len
            end = (arr_size - 1) - i
            nums[start], nums[end] = nums[end], nums[start]

        for i in range(arr_size/2):
            nums[i], nums[(arr_size - 1) - i] = nums[(arr_size - 1) - i], nums[i]
        return nums
#
# class Solution(object):
#     def rotate(self, nums, k):
#         arr_size = len(nums)
#         k %= arr_size
#         moving_len = arr_size - k
#         for i in range(moving_len):
#             nums.append(nums[0])
#             del nums[0]
#         return nums

class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        self.assertEqual(app.rotate([1, 2, 3, 4, 5, 6, 7], 3), [5, 6, 7, 1, 2, 3, 4])
        self.assertEqual(app.rotate([1, 2, 3, 4, 5, 6], 11), [2, 3, 4, 5, 6, 1])


if __name__ == "__main__":
    unittest.main()
