import unittest
import pdb

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

import Queue


class Solution(object):
    def averageOfLevels(self, root):
        """
        :type root: TreeNode
        :rtype: List[float]
        """
        rtn_vals = []
        this_level_value = 0
        this_level_count = 0
        q = Queue.Queue()
        next_queue = Queue.Queue()
        q.put(root)
        while True:
            while q.empty() == False:
                current_node = q.get()
                this_level_value += current_node.val
                this_level_count += 1
                if current_node.left != None:
                    next_queue.put(current_node.left)
                if current_node.right != None:
                    next_queue.put(current_node.right)

            rtn_vals.append(float(this_level_value) / float(this_level_count))

            if next_queue.empty() == False:
                this_level_value = 0
                this_level_count = 0
                q = next_queue
                next_queue = Queue.Queue()
            else:
                return rtn_vals


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
