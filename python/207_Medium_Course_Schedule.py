# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
import time
import tree_util
import tree_helper

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


class Solution(object):
    def run(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        return None


class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = Solution()
        self.tree = tree_helper.Tree()
        self.fixture = []
        self.expected_output = []
        self.tree.load_array(self.fixture)

    def test_default(self):
        self.assertEqual(self.app.run(self.tree.head), self.expected_output)


if __name__ == "__main__":
    unittest.main()