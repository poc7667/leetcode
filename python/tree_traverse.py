# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


class Node:
    def __init__(self, info):  # constructor of class

        self.info = info  # information for node
        self.left = None  # left leef
        self.right = None  # right leef
        self.level = None  # level none defined

    def __str__(self):
        return str(self.info)  # return as string


class Searchtree:
    def __init__(self):  # constructor of class
        self.root = None

    def create(self, val):  # create binary search tree nodes

        if self.root == None:
            self.root = Node(val)
        else:
            current = self.root
            while 1:
                if val < current.info:

                    if current.left:
                        current = current.left
                    else:
                        current.left = Node(val)
                        break

                elif val > current.info:

                    if current.right:
                        current = current.right
                    else:
                        current.right = Node(val)
                        break
                else:
                    break


result = []


def bfs(node):
    if node == None:
        return None
    if node.left == None and node.right == None:
        result.append(node.info)
        return node.info
    result.append(node.info)
    bfs(node.left)
    bfs(node.right)


# ![inline](https://i.imgur.com/TASpD3K.png=300x "Title")

class TestCase(unittest.TestCase):
    def test_default(self):
        tree = Searchtree()
        arr = [8, 3, 1, 6, 4, 7, 10, 14, 13]  # already pre-order

        for i in arr:
            tree.create(i)
        bfs(tree.root)
        print(result)
        self.assertEqual(bfs(tree.root), [8, 3, 1, 6, 4, 7, 10, 14, 13])


if __name__ == "__main__":
    unittest.main()
