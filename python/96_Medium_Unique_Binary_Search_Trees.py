# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """
        a = 1
        b = 1
        for i in range(n+1, 2*n+1):
            a *= i
        for i in range(1, n+1):
            b *= i
        return (a / b) / (n + 1)


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        self.assertEqual(app.numTrees(3), 5)
        self.assertEqual(app.numTrees(2), 2)
        self.assertEqual(app.numTrees(1), 1)


if __name__ == "__main__":
    unittest.main()
