# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def leastInterval(self, tasks, n):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        count_map = dict()
        freq = []
        for i in tasks:
            if i in count_map:
                count_map[i] += 1
            else:
                count_map[i] = 1
        for i in count_map.keys():
            freq.append((i, count_map[i]))
        freq = sorted(freq, key=lambda x: x[1])[::-1]
        inter_slots = freq[0][1]-1
        spaces = (freq[0][1]-1) * n
        remaining = 0
        for i in freq[1:]:
            if i[1] >= inter_slots:
                spaces = spaces - inter_slots
            else:
                spaces -= i[1]
        if remaining >= spaces:
            return 0+len(tasks)
        else:
            return spaces - remaining + len(tasks)


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, False)

