import unittest
import pdb


# Graph traverse and can not duplicated.

class Node(object):
    def __str__(self):
        return self.value

    def __init__(self, board, i, j, value):
        self.i = i
        self.j = j
        self.value = value
        self.links = []
        if i - 1 >= 0:
            self.links.append((i - 1, j))
        if i + 1 < len(board):
            self.links.append((i + 1, j))
        if j - 1 >= 0:
            self.links.append((i, j - 1))
        if j + 1 < len(board[0]):
            self.links.append((i, j + 1))
        # print 'Val:', value, i, j
        # print self.links


import Queue


class Solution(object):

    def find_next(self, visited, node, word):
        visited.append(node)
        for (i, j) in node.links:
            # print 'visited',
            # for kk in visited:
            #     print kk,
            #     print (kk.i, kk.j),
            # print ''
            next_node = self.mat[i][j]
            if next_node not in visited and (next_node.value == word[0]):
                # print 'Next', next_node.value, next_node.i, next_node.j
                if len(word) == 1:
                    return True
                else:
                    # print 'Keep visit', next_node.i, next_node.j
                    if self.find_next(visited[:], next_node, word[1:]):
                        return True

        return False

    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        visited = list()
        self.board = board
        self.map = dict()
        self.mat = []

        # make map
        for i in range(0, len(board)):
            self.mat.append([])

            for j in range(0, len(board[i])):
                val = board[i][j]
                nod = Node(board, i, j, val)

                self.mat[i].append(nod)

                # build link-list map
                if val in self.map:
                    self.map[val].append(nod)
                else:
                    self.map[val] = [nod]

        if word[0] in self.map:
            if len(word) == 1:
                return True
            for curr_node in self.map[word[0]]:
                if self.find_next(visited[:], curr_node, word[1:]):
                    return True
        return False


class MyTestCase(unittest.TestCase):
    def test_something(self):
        inst = Solution()
        # board = [
        #     ['A', 'B', 'C', 'E'],
        #     ['S', 'F', 'C', 'S'],
        #     ['A', 'D', 'E', 'E']
        # ]
        # self.assertEqual(True, inst.exist(board, 'ABCCED'))
        # self.assertEqual(True, inst.exist(board, 'SEE'))
        # self.assertEqual(False, inst.exist(board, 'ABCB'))

        board = [
            ["A", "B", "C", "E"],
            ["S", "F", "E", "S"],
            ["A", "D", "E", "E"]
        ]
        self.assertEqual(False, inst.exist(board, "ABCESEEEFS"))

    # Given word = "ABCCED", return true.
    # Given word = "SEE", return true.
    # Given word = "ABCB", return false.


if __name__ == '__main__':
    unittest.main()
