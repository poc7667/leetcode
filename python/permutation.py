# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


def permutation(nums, start, end):
    if start == end:
        print(nums)
    for i in range(start, end + 1):
        nums[start], nums[i] = nums[i], nums[start]
        permutation(nums, start + 1, end)
        nums[start], nums[i] = nums[i], nums[start]

ins = range(1, 3 + 1)
permutation(ins, 0, len(ins) - 1)

#

#
#
# if __name__ == "__main__":
#     # permutation([1])
#     # permutation([1, 2])
#     ins = range(1, 4)
#     expected_match_len = len(ins)
#     permutation(ins)
