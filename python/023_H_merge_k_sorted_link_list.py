import pdb
import unittest
from common import ListNode
import Queue


def doCompare(nodes):
    try:
        if nodes[0].val < nodes[1].val:
            return 0
        else:
            return 1
    except Exception as e:
        print nodes[0]
        print nodes[1]
        pdb.set_trace()


def doMerge(groups):
    root = ListNode(None)
    while len(groups) > 1:
        nodes = list()
        nodes.append(groups.pop())
        nodes.append(groups.pop())

        # set up root, head, current
        min_idx = doCompare(nodes)
        ptr = nodes[min_idx]
        nodes[min_idx] = nodes[min_idx].next
        root.next = ptr
        current_index = min_idx

        while nodes[0] and nodes[1]:
            min_idx = doCompare(nodes)
            if current_index == min_idx:
                ptr = nodes[min_idx]
                nodes[min_idx] = nodes[min_idx].next
            else:
                if min_idx == 0:
                    ptr.next = nodes[0]
                    ptr = nodes[min_idx]
                    nodes[0] = nodes[0].next
                else:
                    ptr.next = nodes[1]
                    ptr = nodes[min_idx]
                    nodes[1] = nodes[1].next
            current_index = min_idx
        if nodes[0] == None:
            ptr.next = nodes[1]
        else:
            ptr.next = nodes[0]
        groups.append(root.next)
    return root.next


class Solution(object):

    def mergeKLists(self, groups):
        """
        :type groups: List[ListNode]
        :rtype: ListNode
        """
        newGroup = []
        for i in groups:
            if i == None:
                continue
            newGroup.append(i)
        if newGroup == None:
            return None
        if len(newGroup) == 0:
            return None
        if len(newGroup) == 1:
            return newGroup[0]

        q = Queue.Queue()
        for i in newGroup:
            q.put(i)
        while q.qsize() > 1:
            a, b = q.get(), q.get()
            result = doMerge([a, b])
            q.put(result)

        return q.get()


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        lst1 = ListNode.generate([1, 4, 5])
        lst2 = ListNode.generate([2, 3, 4])
        self.assertEqual(ListNode.generate([1, 2, 3, 4, 4, 5]),
                         sol.mergeKLists([lst1, lst2]))

    def test_groups(self):
        sol = Solution()
        lst1 = ListNode.generate([1])
        lst2 = ListNode.generate([2])
        lst3 = ListNode.generate([7, 8])
        lst4 = ListNode.generate([9])
        self.assertEqual(ListNode.generate([1, 2, 7, 8, 9]),
                         sol.mergeKLists([lst1, lst2, lst3, lst4]))


if __name__ == '__main__':
    unittest.main()
