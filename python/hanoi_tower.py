# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print
cnt = 0

def t(nums, s, d, tmp):
    global cnt
    if len(nums) == 1:
        cnt+=1
        # print("move {val} {s}->{d}".format(val=nums[0:1], s=s, d=d))
    else:
        t(nums[1:], s, get_tmp(s, d), d)
        t(nums[0:1], s, d, get_tmp(s, d))
        t(nums[1:], get_tmp(s, d), d, get_tmp(d, get_tmp(s,d)))

def get_tmp(s, d):
    tmp_lst = ["s", "d", "tmp"]
    tmp_lst.remove(s)
    tmp_lst.remove(d)
    return tmp_lst[0]

if __name__ == "__main__":
    init_tower = range(1, 20)[::-1]
    t(init_tower, "s", "d", "tmp")
    print("cnt:"+str(cnt))