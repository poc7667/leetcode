import unittest


def binary_search(nums, t):
    # boundary case
    if len(nums)==0:
        return -1

    start = 0
    end = len(nums) - 1
    mid = len(nums) / 2

    while start + 1 < end:
        if t == nums[mid]:
            return mid
        elif t > nums[mid]:
            start = mid + 1
        else:
            end = mid - 1
        mid = (start + end + 1) / 2
    if nums[start] == t:
        return start
    elif nums[end] == t:
        return end
    else:
        return -1


# def binary_search(nums, t):
#     start = 0
#     end = len(nums) - 1
#     mid = start + (end-start)/2
#     while start + 1 < end:
#         if nums[mid] == t:
#             return mid
#         elif t > nums[mid]:
#             start = mid + 1
#         elif t < nums[mid]:
#             end = mid - 1
#         mid = start + (end-start)/2
#
#     if t==nums[start]:
#         return start
#     if t==nums[end]:
#         return end
#     if t==nums[mid]:
#         return mid
#     return -1

class TestCase(unittest.TestCase):
    def test_default(self):
        self.assertEqual(2, binary_search([1, 2, 4, 6, 8, 9], 4))
        self.assertEqual(-1, binary_search([1, 2, 4, 6, 8, 9], 13))

        self.assertEqual(binary_search([], 3), -1)
        self.assertEqual(binary_search([1, 2], 3), -1)
        self.assertEqual(binary_search([1, 2, 3, 4], 3), 2)
        self.assertEqual(binary_search([1, 2, 3, 4, 5], 3), 2)
        self.assertEqual(binary_search([1, 2, 3, 4], 3), 2)
        self.assertEqual(binary_search([1, 2, 3, 4], 1), 0)
        self.assertEqual(binary_search([1, 2, 3, 4], 4), 3)


if __name__ == "__main__":
    unittest.main()
