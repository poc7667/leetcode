# -*- coding: utf8 -*-
from __future__ import print_function

# Total Accepted: 80769
# Total Submissions: 342424
# Difficulty: Medium
# Given a list, rotate the list to the right by k places, where k is non-negative.
#
# For example:
# Given 1->2->3->4->5->NULL and k = 2,
# return 4->5->1->2->3->NULL.
#


import pdb
import unittest
import linked_list_util

p = print


# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __str__(self):
        if self.next:
            return "{value} next: {next}" \
                .format(value=self.val, next=self.next.val)
        else:
            return "{value} next: {next}" \
                .format(value=self.val, next="NULL")


class Solution(object):
    def get_size(self, head):
        size = 1
        tmp_cur = head
        while tmp_cur.next != None:
            size += 1
            tmp_cur = tmp_cur.next
        return size

    def update_last_node(self, head):
        discover_last_node = head
        while discover_last_node.next != None:
            discover_last_node = discover_last_node.next
        discover_last_node.next = head

    def get_new_head(self, head, size, k):
        new_head = head
        for i in range(size - k):
            new_head = new_head.next
        return new_head

    def swap_two_nodes(self, head):
        new_head = head.next
        new_head.next = head
        head.next = None
        return new_head

    def get_new_tail_node(self, head, new_head):
        new_tail = head
        while new_tail.next != new_head:
            new_tail = new_tail.next
        return new_tail

    def get_tail(self, head):
        cur = head
        while cur.next != None:
            cur = cur.next
        return cur

    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        # Given 1->2->3    ->4->5->NULL and k = 2,
        # return 4->5      ->1->2->3->NULL.
        if head == None or head.next == None:
            return head

        size = self.get_size(head)
        k %= size
        if k == 0:
            return head

        tail = self.get_tail(head)
        new_head = self.get_new_head(head, size, k)
        new_tail = self.get_new_tail_node(head, new_head)
        tail.next = head
        new_tail.next = None

        return new_head



        # if size == 1:
        #     return head
        # if size == 2:
        #     if (k % 2 == 0):
        #         return head
        #     else:
        #         return self.swap_two_nodes()
        #
        #
        # new_head = self.get_new_head(head, size, k)
        #
        # self.update_last_node(head)
        #
        # # move 1st part tail's next pointer to none
        # new_last_node = orig_head
        # for i in range(k):
        #     new_last_node = new_last_node.next
        # new_last_node.next = None
        # pdb.set_trace()
        #
        # return new_head


class TestCase(unittest.TestCase):
    def setup_nodes(self):
        node1 = ListNode(1)
        node2 = ListNode(2)
        node3 = ListNode(3)
        node4 = ListNode(4)
        node5 = ListNode(5)
        node1.next = node2
        node2.next = node3
        node3.next = node4
        node4.next = node5
        return node1

    def setup_case2_fixture(self):
        node1 = ListNode(1)
        node2 = ListNode(2)
        node1.next = node2
        return node1

    def setup_case3_fixture(self):
        node1 = ListNode(1)
        node2 = ListNode(2)
        node3 = ListNode(3)
        node1.next = node2
        node2.next = node3
        return node1

    def test_default(self):
        app = Solution()

        head = self.setup_nodes()
        new_head = app.rotateRight(head, 2)
        self.assertEqual(new_head.val, 4)

        head = self.setup_case2_fixture()
        new_head = app.rotateRight(head, 1)
        self.assertEqual(new_head.val, 2)

        head = self.setup_case2_fixture()
        new_head = app.rotateRight(head, 0)
        self.assertEqual(new_head.val, 1)

        head = self.setup_case3_fixture()
        new_head = app.rotateRight(head, 20000)
        self.assertEqual(linked_list_util.get_last_node(new_head).val, 1)


if __name__ == "__main__":
    unittest.main()
