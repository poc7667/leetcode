# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def numJewelsInStones(self, J, S):
        """
        :type J: str
        :type S: str
        :rtype: int
        """
        ans=0
        for j_ch in J:
            i = 0
            while i < len(S):
                if S[i]==j_ch:
                    ans+=1
                i+=1
        return ans


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(3, sol.numJewelsInStones("aA", "aaAbbbb"))
