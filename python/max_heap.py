# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
from heapq import heappop, heappush

p = print

DUMMY_PARENT = 999999


# Definition for a binary tree node.
def search(nums, x, i):
    if len(nums) > 0:
        if x == nums[i]:
            return i
        elif x < nums[i]:
            rtn_val = search(nums, x, to_left_child_idx(i)) or search(nums, x, to_right_child_idx(i))
            return rtn_val
        else:
            return None
    return None


# def insert(nums, x):
#     nums.append(x)
#     last_index = len(nums) - 1
#     insert_adujst(nums, last_index, x)
#     return nums
#
#
# def insert_adujst(nums, index, x):
#     if index > 0:
#         parent_idx = get_parent_idx(index)
#         if parent_idx >= 0:
#             if nums[parent_idx] < nums[index]:
#                 nums[parent_idx], nums[index] = nums[index], nums[parent_idx]
#                 insert_adujst(nums, parent_idx, x)
#

# def pop(nums, x):
#     # TODO

def max_heap(nums):
    mid = (0 + (len(nums) - 1)) / 2
    for i in range(0, mid + 1)[::-1]:
        adujst(nums, i, len(nums) - 1)
    return nums


def adujst(nums, parent_index, last_index):
    if parent_index >= 0:
        print("nums", nums, nums[parent_index], parent_index)
        left_child_index = to_left_child_idx(parent_index)
        right_child_index = to_right_child_idx(parent_index)
        if left_child_index <= last_index:
            max_child_index = _get_max_child_index(nums, left_child_index, right_child_index, last_index)
            if nums[max_child_index] > nums[parent_index]:
                nums[max_child_index], nums[parent_index] = nums[parent_index], nums[max_child_index]
                adujst(nums, max_child_index, last_index)


def _get_max_child_index(nums, left_child_index, right_child_index, last_index):
    if right_child_index <= last_index:
        max_child_val = max(nums[left_child_index], nums[right_child_index])
        max_child_index = nums.index(max_child_val)
    else:
        max_child_index = left_child_index
    return max_child_index


def get_parent_idx(idx):
    return (idx + 1) / 2 - 1


def to_left_child_idx(idx):
    return (idx + 1) * 2 - 1


def to_right_child_idx(idx):
    return (idx + 1) * 2 + 1 - 1


class TestCase(unittest.TestCase):
    def test_default(self):
        fixture = [21, 90, 3, 36]
        self.assertEqual(max_heap(fixture), [90, 36, 3, 21])
        fixture = [16, 1, 2, 8, 7, 9, 3, 10, 4, 14]
        self.assertEqual(max_heap(fixture), [16, 14, 10, 8, 7, 9, 3, 2, 4, 1])

        # self.assertEqual(max_heap([14]), [14])
        # self.assertEqual(max_heap([5, 14]), [14, 5])
        # self.assertEqual(max_heap([14, 1, 5]), [14, 1, 5])
        # self.assertEqual(max_heap([14, 1, 5, 3]), [14, 3, 5, 1])


    # def test_search(self):
    #     self.assertEqual(search([14, 3, 5, 1], 5, 0), 2)
    #     self.assertEqual(search([14, 3, 5, 1], 1, 0), 3)
    #     self.assertEqual(search([14, 3, 5, 1], 9, 0), None)

    # def test_insert(self):
    #     fixture = [26, 5, 77,49, 13, 90, 50, 100]
    #     h = []
    #     for i in fixture:
    #         heappush(h, i)
    #     h.reverse()
    #     res = max_heap(fixture)
    #     pdb.set_trace()
    #     # self.assertEqual(h, )
    #     pdb.set_trace()
    #     h = []
    #     for i in fixture:
    #         heappush(h, i)
    #     h.reverse()
    #
    #
    #     result = insert(max_heap(fixture[:-1]), 60)
    #     print(result)
    #     self.assertEqual(h, result)
    #
    #     pdb.set_trace()


if __name__ == "__main__":
    unittest.main()
