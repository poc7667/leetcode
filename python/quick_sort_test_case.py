import unittest
import pdb


class QuickSortTestCase(unittest.TestCase):
    def test_something(self, isinstance):
        self.assertEqual(isinstance.qs([3, 7, 1, 5]), [1, 3, 5, 7])
        self.assertEqual([-2321, 0, 7, 222], isinstance.qs([7, -2321, 222, 0]))
        self.assertEqual([-2321, 0, 6, 7, 8, 9, 10, 222, 5245], isinstance.qs([7, 6, 9, 10, 8, -2321, 5245, 222, 0]))
        self.assertEqual([6, 7, 8, 9, 10], isinstance.qs([7, 6, 9, 10, 8]))
        self.assertEqual(
            [1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 8, 9, 9, 9],
            isinstance.qs([3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3])
        )
        self.assertEqual([3], isinstance.qs([3]))
        self.assertEqual([1, 3, 5], isinstance.qs([3, 1, 5]))
        self.assertEqual([3, 5], isinstance.qs([5, 3]))


if __name__ == '__main__':
    unittest.main()
