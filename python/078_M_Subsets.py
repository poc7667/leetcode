import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# class Solution(object):
#     def comb(self, prefix, nums):
#         if len(nums) == 1:
#             self.outputs.append(prefix)
#             self.outputs.append(prefix + [nums[0]])
#         else:
#             self.comb(prefix, nums[1:])
#             self.comb(prefix + [nums[0]], nums[1:])
#
#     def subsets(self, nums):
#         """
#         :type nums: List[int]
#         :rtype: List[List[int]]
#         """
#         self.outputs = []
#         self.comb([], nums)
#         print(self.outputs)
#         return self.outputs


class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        outputs = []

        def doTask(prefix, nums):
            if len(nums) == 1:
                outputs.append(prefix)
                outputs.append(prefix + nums)
                return True
            doTask(prefix + [nums[0]], nums[1:])
            doTask(prefix, nums[1:])

        doTask([], nums)
        return outputs


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(4, sol.subsets([1, 2]))


if __name__ == '__main__':
    unittest.main()
