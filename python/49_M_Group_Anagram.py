import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        mapping = {}
        result = []
        group_index = 0
        for i in range(0, len(strs)):
            word = strs[i]
            sorted_chs = sorted(word)
            sorted_key = ''.join(sorted_chs)
            if sorted_key not in mapping:
                result.append([word])
                mapping[sorted_key] = group_index
                group_index+=1
            else:
                result[mapping[sorted_key]].append(word)
        return result




class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        input = ["eat", "tea", "tan", "ate", "nat", "bat"]
        output = [
            ["ate", "eat", "tea"],
            ["nat", "tan"],
            ["bat"]
        ]
        self.assertEqual(output, sol.groupAnagrams(input))


if __name__ == '__main__':
    unittest.main()
