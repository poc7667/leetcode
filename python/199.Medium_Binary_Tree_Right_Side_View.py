# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

#
# Given a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.
#
# For example:
# Given the following binary tree,
#    1            <---
#  /   \
# 2     3         <---
#  \     \
#   5     4       <---
# You should return [1, 3, 4].
import tree_util

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


class Solution(object):
    def rightSideView(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        output = []
        if root:
            current_lv = [root]
            children_lv = []
            while len(current_lv) > 0:
                output.append(current_lv[-1].val)
                for i in current_lv:
                    if i.left:
                        children_lv.append(i.left)
                    if i.right:
                        children_lv.append(i.right)
                current_lv = children_lv
                children_lv = []
        return output

class TestCase(unittest.TestCase):

    def setUp(self):
        self.app = Solution()
        self.root = tree_util.create_tree([3, 9, 20, None, None, 7, 15])

    def test_default(self):
        self.assertEqual(self.app.rightSideView(self.root),
                         [3, 20, 15])

    # def test_2(self):
    #     self.root = tree_util.create_tree([3, 9])
    #     self.assertEqual(self.app.rightSideView(self.root),
    #                      [3, 9])


if __name__ == "__main__":
    unittest.main()
