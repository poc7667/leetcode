# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print
# 遍历python不定常不定维数组
# http://corpus.hubwiz.com/1010000000582860.html

# A => B
# [119, 35] => [119*100, 35*100]

#
#[[119, 35], [3, 4]] => [[11900, 3500], [11900, 3500]]
#[A!, B!] => [A!, B!]
def convert_coordinates(L):
    return do_convert(L).next()

def do_convert(L):
    if isinstance(L[0], int):
        yield [i*100 for i in L]
    else:
        res = []
        for i in L:
            res.extend([j for j in do_convert(i)])
        yield res

class TestCase(unittest.TestCase):
    def setUp(self):
        None

    def test_default(self):
        coordinates = [119, 35]
        expected_coordinates = [11900, 3500]
        self.assertEqual(convert_coordinates(coordinates), expected_coordinates)

        coordinates = [[119, 35], [3, 4]]
        expected_coordinates = [[11900, 3500], [300, 400]]
        self.assertEqual(convert_coordinates(coordinates), expected_coordinates)
        #
        coordinates = [[[119, 35], [119, 35]], [[119, 35], [119, 35]]]
        expected_coordinates = [[[11900, 3500], [11900, 3500]], [[11900, 3500], [11900, 3500]]]
        self.assertEqual(convert_coordinates(coordinates), expected_coordinates)

if __name__ == "__main__":
    unittest.main()