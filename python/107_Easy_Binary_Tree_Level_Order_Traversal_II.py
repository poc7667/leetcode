# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

# Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).
#
# For example:
# Given binary tree [3,9,20,null,null,15,7],
p = print
import tree_util


class Solution(object):
    def levelOrderBottom(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if root==None:
            return []
        queue = [root]
        next_queue = []
        res = []
        while len(queue) > 0:
            for node in queue:
                if node.left != None and node.left.val != None:
                    next_queue.append(node.left)
                if node.right != None and node.right.val != None:
                    next_queue.append(node.right)
            res.append([i.val for i in queue])
            queue = next_queue
            next_queue = []
        res.reverse()
        return res

class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = Solution()

    def test_default(self):
        root = tree_util.create_tree([3, 9, 20, None, None, 15, 7])
        self.assertEqual(self.app.levelOrderBottom(root), [
            [15, 7],
            [9, 20],
            [3]
        ])
        root = tree_util.create_tree([0, 2, 4, 1, None, 3, -1, 5, 1, None, 6, None, 8])
        self.assertEqual(self.app.levelOrderBottom(root), [
            [[5, 1, 6, 8], [1, 3, -1], [2, 4], [0]]
        ])



if __name__ == "__main__":
    unittest.main()
