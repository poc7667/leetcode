import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def validChar(self, ch):
        return int(ch) > 0 and int(ch) <= 26

    def toCh(self, ch):
        return chr(int(ch) + ord('A') - 1)

    # def doDecode(self, prefix, s):
    #     if len(s) == 0:
    #         self.results.append(prefix)
    #         return True
    #     if len(s) == 1:
    #         if self.validChar(s[0]):
    #             self.results.append(prefix + self.toCh(s[0]))
    #             return True
    #         else:
    #             return False
    #
    #     if self.validChar(s[0]) and self.validChar(s[1]):
    #         self.doDecode(prefix + self.toCh(s[0]), s[1:])
    #     else:
    #         if self.validChar(s[0]) == False:
    #             return False
    #     if (10 * int(s[0]) + int(s[1])) <= 26:
    #         newPrefix = prefix + self.toCh(s[0:2])
    #         if len(s) == 2:
    #             self.doDecode(newPrefix, '')
    #         elif self.validChar(s[2]):
    #             self.doDecode(prefix + self.toCh(s[0:2]), s[2:])

    def numDecodings(self, s):
        # print s
        prefix = ''
        q = Queue.Queue()
        results = []
        q.put((prefix, s))
        while not q.empty():
            _pre, _s = q.get()
            if len(_s) == 0:
                continue
            if len(_s) == 1:
                if self.validChar(_s[0]):
                    results.append(_pre + self.toCh(_s[0]))
                continue

            if self.validChar(_s[0]) and self.validChar(_s[1]):
                q.put((_pre+ self.toCh(_s[0]), _s[1:]))
            else:
                if self.validChar(_s[0]) == False:
                    continue
            if (10 * int(_s[0]) + int(_s[1])) <= 26:
                newPrefix = prefix + self.toCh(_s[0:2])
                if len(_s) == 2:
                    results.append(newPrefix)
                elif self.validChar(_s[2]):
                    q.put((prefix + self.toCh(_s[0:2]), _s[2:]))

        # self.doDecode('', s)
        return len(results)


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(0, sol.numDecodings('0'))
        self.assertEqual(0, sol.numDecodings('00'))
        self.assertEqual(2, sol.numDecodings('12'))
        self.assertEqual(3, sol.numDecodings('226'))
        self.assertEqual(3, sol.numDecodings('4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948'))


if __name__ == '__main__':
    unittest.main()
