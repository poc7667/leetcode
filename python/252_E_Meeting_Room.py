# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def canAttendMeetings(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: bool
        """

        if len(intervals) <= 1:
            return True

        sorted_meetings = sorted(intervals, key= lambda  x: x.start)
        outputs = []
        for i in sorted_meetings:
            if len(outputs)==0:
                outputs.append(i)
            else:
                last = outputs[-1]
                if last.end > i.start:
                    print outputs, i
                    return False
                else:
                    outputs.append(i)

        return True

class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, False)
