import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        mapping = {
            ')': '(',
            '}': '{',
            ']': '['
        }
        stk = []
        for i in s:
            if i not in mapping:
                stk.append(i)
            else:
                if len(stk) == 0:
                    return False
                else:
                    value = stk[-1]
                    del stk[-1]
                    if value != mapping[i]:
                        return False

        if len(stk) == 0:
            return True
        else:
            return False


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, sol.isValid('()'))
        self.assertEqual(True, sol.isValid("[]"))
        self.assertEqual(False, sol.isValid('[(}]'))


if __name__ == '__main__':
    unittest.main()
