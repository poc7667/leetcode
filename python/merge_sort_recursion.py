# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


def merge_sort(nums):
    worker(nums, 0, len(nums) - 1)
    return nums


def worker(nums, l, r):
    if l < r:
        mid = l + (r - l) / 2
        worker(nums, l, mid)
        worker(nums, mid + 1, r)
        merge(nums, l, mid, r)


def merge(nums, l, m, r):
    tmp_nums = []
    L = nums[l:m + 1]
    R = nums[m + 1:r + 1]
    while len(L) > 0 and len(R) > 0:
        if L[0] <= R[0]:
            tmp_nums.append(L[0])
            del L[0]
        else:
            tmp_nums.append(R[0])
            del R[0]
    if len(L) == 0:
        tmp_nums.extend(R)
    elif len(R) == 0:
        tmp_nums.extend(L)

    for i in range(l, r + 1):
        nums[i] = tmp_nums[i - l]


class TestCase(unittest.TestCase):
    def test_default(self):
        self.assertEqual(merge_sort([1, 4, 2, 3]), [1, 2, 3, 4])
        self.assertEqual(merge_sort([9, 8, 7, 6, 5, 4, 3, 2, 1]), [1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.assertEqual(merge_sort([9, 8, 7, 6, 6, 5, 23, 1]), [1, 5, 6, 6, 7, 8, 9, 23])


if __name__ == "__main__":
    unittest.main()
