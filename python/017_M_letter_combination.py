import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def __init__(self):
        mapx = {'2': ['a', 'b', 'c'],
                '3': ['d', 'e', 'f'],
                '4': ['g', 'h', 'i'],
                '5': ['j', 'k', 'l'],
                '6': ['m', 'n', 'o'],
                '7': ['p', 'q', 'r', 's'],
                '8': ['t', 'u', 'v'],
                '9': ['w', 'x', 'y', 'z']}
        self.mapping = mapx
        self.results = []

    def expand(self, digits, prefix):
        if len(digits) == 0:
            self.results.append(prefix)
            return True
        else:
            for i in self.mapping[digits[0]]:
                self.expand(digits[1:], prefix + i)

    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if 0 == len(digits):
            return []
        self.expand(digits, '')
        return self.results


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"], sol.letterCombinations('23'))


if __name__ == '__main__':
    unittest.main()
