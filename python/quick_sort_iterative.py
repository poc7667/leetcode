# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


class Solution(object):

    def iterative_sort(self, nums):
        Q = [nums]
        while not self.sort_done(Q):
            tmp_Q = []
            for i in Q:
                if len(i) > 1:
                    self.sort(i, tmp_Q)
                else:
                    tmp_Q.append(i)
            Q = tmp_Q        
        return [i[0] for i in Q]

    def sort_done(self, q):
        for i in q:
            if len(i) > 1:
                return False
        return True

    def sort(self, nums, tmp_Q):
        left_bound = 0
        last_idx = len(nums)-1
        for i in range(0, last_idx):
            if nums[i] < nums[last_idx]:
                nums[i], nums[left_bound] = nums[left_bound], nums[i]
                left_bound+=1
        nums[last_idx], nums[left_bound] = nums[left_bound], nums[last_idx]
        # boundary problem 要額外注意跟小心
        if 0 < left_bound and left_bound < last_idx:            
            tmp_Q.append(nums[0:left_bound])
            tmp_Q.append([nums[left_bound]])
            tmp_Q.append(nums[left_bound+1:])
        elif 0==left_bound:            
            tmp_Q.append([nums[left_bound]])
            tmp_Q.append(nums[left_bound+1:])
        elif left_bound == last_idx:
            tmp_Q.append(nums[0:left_bound])
            tmp_Q.append([nums[last_idx]])            
        

class TestCase(unittest.TestCase):
    def setUp(self):
        self.sol = Solution()

    def test_quick_sort_iterative3(self):
        self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], self.sol.iterative_sort([5, 6, 7, 3, 4, 1, 8, 2, 9]))

    def test_quick_sort_iterative4(self):
        self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], self.sol.iterative_sort([2, 6, 9, 3, 4, 1, 8, 5, 7]))

    def test_quick_sort_iterative5(self):
        self.assertEqual([1, 2, 3, 4, 5, 8, 9], self.sol.iterative_sort([5, 3, 4, 1, 8, 2, 9]))

    def test_quick_sort_iterative6(self):
        self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], self.sol.iterative_sort([9, 2, 7, 5, 8, 1, 4, 3, 6]))

    def test_quick_sort_iterative7(self):
        self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], self.sol.iterative_sort([7, 1, 5, 8, 2, 6, 9, 3, 4]))


if __name__ == "__main__":
    unittest.main()
