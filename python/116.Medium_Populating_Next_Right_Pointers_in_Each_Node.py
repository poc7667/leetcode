# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")



class Solution(object):
    def connect(self, root):
        """
        :type root: TreeLinkNode
        :rtype: nothing
        """
        def create_right_links():
            for i in xrange(len(current_nodes)-1):
                current_nodes[i].next = current_nodes[i+1]
            current_nodes[-1].next = None

        def create_children_level(node):
            if node.left:
                    children_nodes.append(node.left)
            if node.right:
                children_nodes.append(node.right)

        if root:
            current_nodes = [root]
            children_nodes = []
            while len(current_nodes) > 0 :
                create_right_links()
                for node in current_nodes:
                    create_children_level(node)
                current_nodes = children_nodes
                children_nodes = []



if __name__ == "__main__":
    unittest.main()