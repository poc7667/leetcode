# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print



class Solution(object):
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if root:
            self.invertTree(root.left)
            self.invertTree(root.right)
            root.left, root.right = root.right, root.left
        return root

class TestCase(unittest.TestCase):
    def setUp(self):
        app = Solution()

    def test_default(self):
        self.assertEqual()


if __name__ == "__main__":
    unittest.main()