# -*- coding: utf8 -*-
import Queue
import pdb
import unittest


class Solution(object):
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if root == None:
            return []
        if root.left == None and root.right ==None:
            return [[root.val]]
        current_q = Queue.Queue()
        next_q = Queue.Queue()
        next_q.put(root)
        outputs = []
        while not next_q.empty():
            this_level = []
            while not next_q.empty():
                current_q.put(next_q.get())
            while not current_q.empty():
                current_node = current_q.get();
                this_level.append(current_node.val)
                if current_node.left:
                    next_q.put(current_node.left)
                if current_node.right:
                    next_q.put(current_node.right)
            outputs.append(this_level)
        return outputs



class TestCase(unittest.TestCase):
    def setUp(self):
        app = Solution()

    def test_default(self):
        self.assertEqual()


if __name__ == "__main__":
    unittest.main()