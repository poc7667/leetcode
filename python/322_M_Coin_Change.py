import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):

    def getMin(self, paths):
        if len(paths) == 0:
            return []
        min = None
        for i in paths:
            if min == None:
                min = [len(i), i]
            elif len(i) < min[0]:
                min = [len(i), i]

        if min != None:
            return min[1]
        else:
            return None

    def doTask(self, coins, remaining, path):
        if remaining == 0:
            return path
        elif remaining in coins:
            path.append(remaining)
            return path
        else:
            paths = []
            for i in coins:
                if (remaining - i) >= 0:
                    new_path = list(path)
                    new_path.append(i)
                    paths.append(self.doTask(coins, remaining - i, new_path))
            min_path = self.getMin(paths)
            return min_path

    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        values = dict()
        values[0] = 1
        for coin in coins:
            values[coin] = 1

        if amount == 0:
            return 0
        if amount < 0:
            return -1

        for i in range(1, amount + 1):
            if i not in values:
                results = []
                for coin in coins:
                    if (i - coin) in values:
                        results.append(1 + values[i - coin])
                if len(results) > 0:
                    values[i] = min(results)

        if amount in values:
            return values[amount]
        else:
            return -1


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(-1, sol.coinChange([2], 3))
        self.assertEqual(3, sol.coinChange([1, 2, 5], 11))
        self.assertEqual(3, sol.coinChange([1, 2, 5], 100))


if __name__ == '__main__':
    unittest.main()
