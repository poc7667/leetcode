import unittest
import pdb

def sequential_search(nums, target):
    for i in range(0, len(nums)):
        if nums[i] == target:
            return i
    return -1

class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        for i in range(0, len(nums)):
            residual_value = target - nums[i]
            foundIndex = sequential_search(nums[i+1: len(nums)], residual_value)
            if foundIndex == -1:
                continue
            else:
                return [i, (i+1)+ foundIndex]
        return [-1,-1]

class MyTestCase(unittest.TestCase):
    def test_sample(self):
        sol = Solution()
        # self.assertEqual([0, 2], sol.twoSum([1, 2, 3], 4))
        # self.assertEqual([1, 2], sol.twoSum([1234, 5678, 9012], 14690))

        # Not sorted
        self.assertEqual([0, 3], sol.twoSum([0, 4, 3, 0], 0))

    # def test_not_found(self):
    #     self.assertEqual(
    #         binary_search([1, 2, 3, 4], 5),
    #         -1)
    #
    # def test_found(self):
    #     self.assertEqual(
    #         binary_search([1, 2, 3, 4], 2),
    #         1)
    #     self.assertEqual(
    #         binary_search([1, 2], 2),
    #         1)
    #
    #     self.assertEqual(
    #         binary_search([0,3,4], 4),
    #         2)


if __name__ == '__main__':
    unittest.main()
