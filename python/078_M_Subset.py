import unittest
import pdb


class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """

        # def pick(nums):
        #     for i, val in enumerate(self.results):
        #         pick(nums[i+1:], current_path[:]+[val])
        #         pick(nums[i + 1:], current_path[:])
        self.results = [[]]
        # pick(nums, self.results)
        for i, val in enumerate(nums):
            for j in range(len(self.results)):
                j_val = self.results[j]
                self.results.append(j_val+[val])

        return self.results


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(False, sol.subsets([1,2,3]))

# if __name__ == '__main__':
#    unittest.main()
