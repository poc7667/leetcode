import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)

# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

#
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """

        if len(intervals)==0:
            return []
        new_intervals = []

        intervals = sorted(intervals, key= lambda x: x.start)[::-1]

        last_item = None
        while len(intervals) > 0:
            if last_item == None:
                last_item = intervals.pop()
                if len(intervals) == 0:
                    continue
            current_item = intervals.pop()
            if last_item.end >= current_item.start:
                if last_item.end >= current_item.end:
                    last_item = Interval(last_item.start, last_item.end)
                else:
                    last_item = Interval(last_item.start, current_item.end)
                continue
            else:
                new_intervals.append(last_item)
            last_item = current_item

        new_intervals.append(last_item)
        return new_intervals




class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual([[1,6],[8,10],[15,18]],
                         sol.merge([[1,3],[2,6],[8,10],[15,18]]))
        self.assertEqual([[1,5]],
                         sol.merge([[1,4],[4,5]]))

if __name__ == '__main__':
    unittest.main()
