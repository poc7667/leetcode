# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if len(nums) > 0:
            start = 0
            end = len(nums) - 1
            mid = (start + end) / 2
            node = TreeNode(nums[mid])
            if mid != start:
                node.left = self.sortedArrayToBST(nums[:mid])
            if mid != end:
                node.right = self.sortedArrayToBST(nums[mid + 1:])
            return node

class TestCase(unittest.TestCase):
    def test_default(self):
        self.assertEqual()


if __name__ == "__main__":
    unittest.main()
