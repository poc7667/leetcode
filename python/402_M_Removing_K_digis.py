# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# 思路: 原本想從頭開始兩兩一組，刪除最大數字，目前發現要三三一組?

# 下面解答思路
# public class Solution {
#     public String removeKdigits(String num, int k) {
#         int digits = num.length() - k;
#         char[] stk = new char[num.length()];
#         int top = 0;
#         // k keeps track of how many characters we can remove
#         // if the previous character in stk is larger than the current one
#         // then removing it will get a smaller number
#         // but we can only do so when k is larger than 0
#         for (int i = 0; i < num.length(); ++i) {
#             char c = num.charAt(i);
#             while (top > 0 && stk[top-1] > c && k > 0) {
#                 top -= 1;
#                 k -= 1;
#             }
#             stk[top++] = c;
#         }
#         // find the index of first non-zero digit
#         int idx = 0;
#         while (idx < digits && stk[idx] == '0') idx++;
#         return idx == digits? "0": new String(stk, idx, digits - idx);
#     }
# }


class Solution(object):

    def pickNumber(self, num):
        print(num)
        min = None
        idx = None
        for i, val in enumerate(num):
            print 'min', min, 'val', val
            if min == None:
                min = val
                idx = 0
            elif val < min:
                return i-1
            else:
                min = val
                idx = i

        return idx

    def removeKdigits(self, num, k):
        """
        :type num: str
        :type k: int
        :rtype: str
        """
        if len(num) == k:
            return "0"
        remaining = k
        digis = [str(i) for i in num]
        i = 0
        while remaining > 0:
            candidate = i+self.pickNumber(digis[i:])
            del digis[candidate]
            remaining -=1
            i+=1
        return str(int(''.join(digis)))


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        # self.assertEqual('39', sol.removeKdigits('539', 1))
        self.assertEqual('338', sol.removeKdigits('3398', 1))
        self.assertEqual('399', sol.removeKdigits('5399', 1))
        self.assertEqual('138', sol.removeKdigits('1398', 1))
        self.assertEqual('33', sol.removeKdigits('339', 1))
        self.assertEqual('1219', sol.removeKdigits('1432219', 3))
        self.assertEqual('200', sol.removeKdigits('10200', 1))
        self.assertEqual('107', sol.removeKdigits('1107', 1))
        self.assertEqual('327', sol.removeKdigits('3427', 1))

