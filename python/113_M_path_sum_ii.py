# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution(object):
    def __init__(self):
        self.res = []
        self.target = None

    def dfs(self, root, path, current_sum):
        # print ('path', path)
        if (not root.left) and (not root.right):
            if current_sum == self.target:
                self.res.append(path)
            return None

        if root.left:
            self.dfs(root.left, path + [root.left.val], current_sum + root.left.val)
        if root.right:
            self.dfs(root.right, path + [root.right.val], current_sum + root.right.val)

    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: List[List[int]]
        """
        self.target = sum
        if not root:
            return []
        self.dfs(root, [root.val], root.val)
        return self.res
