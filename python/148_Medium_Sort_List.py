# -*- coding: utf8 -*-
from __future__ import print_function
import sys
import pdb
import unittest


# sys.setrecursionlimit(55)

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{x} -> {y}".format(x=self.val, y=self.next.val)
        else:
            return "{x} -> {y}".format(x=self.val, y="None")

    def __str__(self):
        if self.next != None:
            return "{x} -> {y}".format(x=self.val, y=self.next.val)
        else:
            return "{x} -> {y}".format(x=self.val, y="None")


def ls(head):
    cnt = 100
    tmp_head = head
    results = []
    while tmp_head != None:
        print("{value}->".format(value=tmp_head.val), end=" ")
        results.append(tmp_head.val)
        tmp_head = tmp_head.next
        cnt -= 1
        if cnt < 0: break
    print("None")
    return results


class Solution(object):
    def sortList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head == None or head.next == None:
            return head
        size = self.get_total_link_size(head)
        return self.sortHelper(head, size - 1)

    def sortHelper(self, wall, offset_to_bound):
        if offset_to_bound == 0:
            return wall  # 超級tricky的終止條件, 要不然會找不到left node
        if offset_to_bound >= 1:
            ls(wall)
            start_head = wall
            wall = wall
            pivot = self.get_node(start_head, offset_to_bound)
            for curr in range(0, offset_to_bound):
                node = self.get_node(start_head, curr)
                if node.val < pivot.val:
                    start_head = self.swap_nodes(start_head, wall, node)
                    wall = node.next
                    self.move_wall(wall)

            if wall != pivot:
                start_head = self.swap_nodes(start_head, wall, pivot)
                wall = pivot.next

            offset = (self.offset(wall, self.get_node(start_head, offset_to_bound)))
            self.sortHelper(wall, offset)
            node_before_pivot = self.prev(start_head, pivot)
            left_part_size = self.offset(start_head, node_before_pivot)
            left = self.sortHelper(start_head, left_part_size)
            return left

    def get_total_link_size(self, head):
        tmp_head = head
        size = 1
        while tmp_head.next != None:
            tmp_head = tmp_head.next
            size += 1
        return size

    def get_segment_link_size(self, start, end):
        size = 0
        tmp_head = start
        while tmp_head != end:
            tmp_head = tmp_head.next
            size += 1
        return size

    def offset(self, l, r):
        if self.inOrderNodes(l, r) == False:
            l, r = r, l
        offset = 0
        if l == r:
            return offset
        start = l

        while start != r:
            start = start.next
            offset += 1
        return offset

    def get_node(self, head, offset):
        tmp_head = head
        for i in range(offset):
            tmp_head = tmp_head.next
        return tmp_head

    def move_wall(self, wall):
        wall = wall.next

    def swap_nodes(self, head, n1, n2):  # n1 should come before n2
        dummy_node = ListNode("Dummy")
        dummy_node.next = head
        if self.inOrderNodes(n1, n2) == False:
            n1, n2 = n2, n1
        pn1 = self.prev(dummy_node, n1)
        pn2 = self.prev(dummy_node, n2)
        if n1 == n2:
            return dummy_node.next
        elif n1.next == n2:
            n1.next = n2.next
            n2.next = n1
            pn1.next = n2
        else:
            n1.next, n2.next = n2.next, n1.next
            pn1.next, pn2.next = pn2.next, pn1.next
        return dummy_node.next

    def inOrderNodes(self, n1, n2):
        tmp_node = n1
        while tmp_node != None:
            if tmp_node == n2:
                return True
            else:
                tmp_node = tmp_node.next
        return False

    def prev(self, head, node):
        if head == node:
            return head
        cur = head
        while cur.next != node:
            cur = cur.next
        return cur


def create_link_list(nums):
    list = []
    for i in nums:
        list.append(ListNode(i))
    for i in range(len(list) - 1):
        list[i].next = list[i + 1]
    return list


class TestCase(unittest.TestCase):
    # def test_default(self):
    #     app = Solution()
    #     n1 = ListNode(1)
    #     n2 = ListNode(2)
    #     n1.next = n2
    #     self.assertEqual(app.swap_nodes(n1, n1, n2), (None, 1))

    def test_0(self):
        app = Solution()
        # test_fixture = [3, 1]
        # expected = [1, 3]
        # n1 = app.sortList(create_link_list(test_fixture)[0])
        # self.assertEqual(ls(n1), expected)
        #
        # test_fixture = [3]
        # expected = [3]
        # n1 = app.sortList(create_link_list(test_fixture)[0])
        # self.assertEqual(ls(n1), expected)
        #
        # test_fixture = [3, 7, 1, 5]
        # expected = [1, 3, 5, 7]
        # n1 = app.sortList(create_link_list(test_fixture)[0])
        # self.assertEqual(ls(n1), expected)
        #
        # test_fixture = [3, 3, 5, 4, 8]
        # expected = [3,3,4,5,8]
        # n1 = app.sortList(create_link_list(test_fixture)[0])
        # self.assertEqual(ls(n1), expected)

        test_fixture = [3, 1, 4, 1, 5, 9, 2]
        expected = [1, 1, 2, 3, 4, 5, 9]
        # test_fixture = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3]
        # expected = [1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 8, 9, 9, 9]
        n1 = app.sortList(create_link_list(test_fixture)[0])
        self.assertEqual(ls(n1), expected)

        # def test_1(self):
        #     app = Solution()


        # def test_2(self):
        #     app = Solution()
        #     test_fixture = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3]
        #     expected = [1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 8, 9, 9, 9]
        #     n1 = app.sortList(create_link_list(test_fixture)[0])
        #     self.assertEqual(ls(n1), expected)

        # def test_2(self):
        #     app = Solution()
        #     nodes = create_link_list([3, 1])
        #     n1 = app.sortList(nodes[0])
        #     ls(n1)


if __name__ == "__main__":
    unittest.main()


# https://github.com/haoliangx/Leet-Code/blob/f1f3b09fe32a54204ba57e96098d9bd31a44be27/src/P-148-Sort-List.py