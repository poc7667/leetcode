import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):

    def doMerge(self, nums, left, right):
        if right - left == 1:
            if nums[left] > nums[right]:
                nums[left], nums[right] = nums[right], nums[left]

        tmp_nums = []
        start = left
        group_size = right - left
        left_bound = left + group_size - 1
        right_bound = right + group_size - 1

        if right_bound > len(nums) - 1:
            right_bound = len(nums) - 1

        while left <= left_bound and right <= right_bound:
            if nums[left] < nums[right]:
                tmp_nums.append(nums[left])
                left += 1
            else:
                tmp_nums.append(nums[right])
                right += 1

        while left <= left_bound:
            tmp_nums.append(nums[left])
            left+=1
        while right <= right_bound:
            tmp_nums.append(nums[right])
            right+=1


        for i in range(len(tmp_nums)):
            nums[i + start] = tmp_nums[i]

    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """

        group_size = 1
        if len(nums) > 1:
            while True:
                num_of_groups = len(nums) / group_size
                if len(nums) % group_size != 0:
                    num_of_groups += 1

                for i in range(0, num_of_groups, 2):
                    if (i + 1) * group_size < len(nums):
                        self.doMerge(nums, i * group_size, (i + 1) * group_size)

                group_size *= 2
                if group_size > len(nums):
                    self.doMerge(nums, 0, group_size/2)
                    break
        return nums


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        # self.assertEqual([5, 8], sol.doMerge([8,5], 0, 1, 1))
        # self.assertEqual([8,5,1,2], sol.doMerge([8,5, 2, 1], 2, 3, 1))

        self.assertEqual([0, 1,2], sol.sortColors([2, 0, 1]))
        self.assertEqual([0, 0, 1, 1, 2, 2], sol.sortColors([2, 0, 2, 1, 1, 0]))


if __name__ == '__main__':
    unittest.main()
