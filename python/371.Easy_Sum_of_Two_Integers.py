# Calculate the sum of two integers a and b, but you are not allowed to use the operator + and -.
#
# Example:
# Given a = 1 and b = 2, return 3.


import unittest


class Solution(object):
    def getSum(self, a, b):
        """
        :type a: int
        :type b: int
        :rtype: int
        """
        while True:
            carry = (a & b) << 1
            sum = a ^ b
            a = carry
            b = sum
            print(a, b)
            if carry == 0:
                break
        return b


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()

        self.assertEqual(app.getSum(3, 5), 8)
        self.assertEqual(app.getSum(-1, 1), 0)


if __name__ == "__main__":
    unittest.main()
