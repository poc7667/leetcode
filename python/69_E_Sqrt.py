import unittest
import pdb
import Queue


class Solution(object):

    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """

        i = x
        while i * i > x:
            i /= 2


        while True:
            current_val = i * i
            if current_val == x:
                return i
            if current_val > x:
                return i - 1
            i += 1



class MyTestCase(unittest.TestCase):
    def test_something(self):
        inst = Solution()
        self.assertEqual(2, inst.mySqrt(4))
        self.assertEqual(2, inst.mySqrt(8))
        # self.assertEqual(False, inst.mySqrt(1007))
        # self.assertEqual(False, inst.mySqrt(1007204570))
        # self.assertEqual(False, True)

    # Given word = "SEE", return true.
    # Given word = "ABCB", return false.


if __name__ == '__main__':
    unittest.main()
