# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
from heapq import heappush, heappop

p = print

def kruskal(graph):
    node_set = {}
    result = set([])
    edge_heap = build_min_edge_heap(graph)
    for i in range(len(edge_heap)):
        edge = heappop(edge_heap)
        if not_cyclic(node_set, edge):
            result.add(edge)
    return result

def build_min_edge_heap(graph):
    edge_heap = []
    for i in graph['edges']:
        heappush(edge_heap, i)
    return edge_heap

def not_cyclic(s, edge):
    node1, node2 = edge[1:]
    if (node1 not in s) and (node2 not in s):
        s[node1] = node1
        s[node2] = node1
    elif both_nodes_are_in_set(s, edge):
        if s[node1] == s[node2]:
            return False  # add this will make set cyclic
        else:  # update node2 set
            updateValue(s, s[node1], s[node2])
    else:  # one is in SET, the other is not
        s[node1] = node1
        s[node2] = node1
    return True

def both_nodes_are_in_set(s, e):
    node1, node2 = e[1:]
    if node1 in s and node2 in s:
        return True
    else:
        return False


def updateValue(s, old_val, new_val):
    for k, v in s.iteritems():
        if v == old_val:
            s[k] = new_val


class TestCase(unittest.TestCase):
    def test_default(self):
        graph = {
            'vertices': ['A', 'B', 'C', 'D', 'E', 'F'],
            'edges': set([
                (1, 'A', 'B'),
                (5, 'A', 'C'),
                (3, 'A', 'D'),
                (4, 'B', 'C'),
                (2, 'B', 'D'),
                (1, 'C', 'D'),
            ])
        }
        minimum_spanning_tree = set([
            (1, 'A', 'B'),
            (2, 'B', 'D'),
            (1, 'C', 'D'),
        ])
        self.assertEqual(kruskal(graph), minimum_spanning_tree)

if __name__ == "__main__":
    unittest.main()
