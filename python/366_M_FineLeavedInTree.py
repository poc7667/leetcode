# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def get_leaves(self, root):
        if root == None:
            return -1
        child_lv = max([self.get_leaves(root.left), self.get_leaves(root.right)])
        this_lv = child_lv +1
        if this_lv in self.nodes:
            self.nodes[this_lv].append(root.val)
        else:
            self.nodes[this_lv] = [root.val]
        return child_lv+1

    def findLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        self.nodes = {}
        ans = []
        max_level = self.get_leaves(root)
        for i in range(max_level+1):
            ans.append(self.nodes[i])
        return ans






class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, False)
