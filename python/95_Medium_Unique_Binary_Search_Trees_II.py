# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


    def __repr__(self):
        if self.val:
            return "{value}".format(value=self.val)
        else:
            return "nil"

class Solution(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        self.preorder_seq = []
        self.results = []
        self.generate_preorder_seq(range(1, n + 1), 0, n - 1)
        inorder_seq = [i + 1 for i in xrange(n)]
        for i in self.preorder_seq:
            try:
                self.results.append(self.create_tree(i, inorder_seq))
            except:
                pass
        return self.results

    def generate_preorder_seq(self, nums, start, end):
        def _swap(nums, i, j):
            nums[i], nums[j] = nums[j], nums[i]

        if start == end:
            this_res = []
            for i in nums:
                this_res.append(i)
            self.preorder_seq.append(this_res)
        else:
            for i in range(start, end + 1):
                _swap(nums, start, i)
                self.generate_preorder_seq(nums, start + 1, end)
                _swap(nums, start, i)

    def create_tree(self, preorder, inorder):
        if len(preorder) > 0:
            root = TreeNode(preorder[0])
            left_part_len = inorder.index(preorder[0])
            right_part_len = len(inorder[inorder.index(preorder[0]):])
            if left_part_len > 0:
                root.left = self.create_tree(preorder[1: left_part_len + 1], inorder[0: left_part_len])
            if right_part_len > 0:
                root.right = self.create_tree(preorder[left_part_len + 1:], inorder[left_part_len + 1:])

            return root
        else:
            return None


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        trees = app.generateTrees(4)
        pdb.set_trace()


if __name__ == "__main__":
    unittest.main()
