import unittest
import pprint

pp = pprint.PrettyPrinter(indent=4)
import pdb
import time



class Solution(object):

    def qs(self, nums):
        if len(nums) <= 1:
            return nums
        if len(nums) == 2:
            if nums[0] > nums[1]:
                return [nums[1], nums[0]]
            else:
                return nums
        wall = 0
        for i in range(0, len(nums) - 1):
            if nums[i] < nums[-1]:
                self.swap(nums, wall, i)
                wall += 1

        # swap
        self.swap(nums, wall, -1)

        if wall == 0:
            return [nums[0]] + self.qs(nums[1:])
        elif wall == len(nums) - 1:
            return self.qs(nums[0: len(nums) - 1]) + [nums[-1]]
        else:
            return self.qs(nums[0:wall]) + [nums[wall]] + self.qs(nums[wall + 1:len(nums)])

    def swap(self, nums, i, j):
        temp = nums[i]
        nums[i] = nums[j]
        nums[j] = temp

    # def two_sum(self, nums, target):
    #     results = []
    #     for (i, val) in enumerate(nums):
    #         new_nums = nums[:]
    #         del new_nums[i]
    #         remaining = target - val
    #         if remaining in new_nums:
    #             results.append([val, remaining])
    #     return results
    #
    # def threeSum(self, nums):
    #     """
    #     :type nums: List[int]
    #     :rtype: List[List[int]]
    #     """
    #     # a+b+c = 0
    #     # b+c = -a
    #     results = []
    #     value_set = set()
    #     self.qs(nums)
    #     for (idx, val) in enumerate(nums):
    #         if val in value_set:
    #             continue
    #         else:
    #             value_set.add(val)
    #         new_nums = nums[:]
    #         del new_nums[idx]
    #         two_sum_results = self.two_sum(new_nums, -val)
    #         if len(two_sum_results):
    #             for res in two_sum_results:
    #                 ans = (res + [val])
    #                 ans.sort()
    #                 if ans not in results:
    #                     results.append(ans)
    #     return results

    def threeSum(self, nums):
        results = []
        two_sum_mapping = dict()
        two_sums_set = set()
        for i in range(0, len(nums) - 1):
            for j in range(i + 1, len(nums)):
                if (nums[i], nums[j]) not in two_sums_set:
                    sum_val = nums[i] + nums[j]
                    _pair = (i, j)
                    if sum_val in two_sum_mapping:
                        two_sum_mapping[sum_val].append(_pair)
                    else:
                        two_sum_mapping[sum_val] = [_pair]
        for i in range(len(nums)):
            if -nums[i] in two_sum_mapping:
                for pair in two_sum_mapping[-nums[i]]:
                    if i in pair:
                        continue
                    else:
                        ans = sorted([nums[i]] + [nums[pair[0]], nums[pair[1]]])
                        if ans not in results:
                            results.append(ans)

        return results


class MyTestCase(unittest.TestCase):

    def test_quick_sort(self):
        sol = Solution()
        self.assertEqual([], sol.qs([]))
        self.assertEqual([1, 3, 5, 7], sol.qs([3, 7, 1, 5]))
        self.assertEqual([-2321, 0, 7, 222], sol.qs([7, -2321, 222, 0]))
        self.assertEqual([-2321, 0, 6, 7, 8, 9, 10, 222, 5245], sol.qs([7, 6, 9, 10, 8, -2321, 5245, 222, 0]))
        self.assertEqual([6, 7, 8, 9, 10], sol.qs([7, 6, 9, 10, 8]))
        self.assertEqual(
            [1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 8, 9, 9, 9],
            sol.qs([3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3])
        )
        self.assertEqual([3], sol.qs([3]))
        self.assertEqual([1, 3, 5], sol.qs([3, 1, 5]))
        self.assertEqual([3, 5], sol.qs([5, 3]))

    #
    def test_something(self):
        sol = Solution()
        nums = [-1, 0, 1, 2, -1, -4]

        self.assertEqual([
            [-1, 0, 1],
            [-1, -1, 2]
        ], sol.threeSum(nums))

        nums = [-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6]
        self.assertEqual([
            [-4, -2, 6], [-4, 0, 4], [-4, 1, 3], [-4, 2, 2], [-2, -2, 4], [-2, 0, 2]
        ], sol.threeSum(nums))

        nums = [-15, 6, 7, 0, -14, -5, -3, -10, -14, 1, -14, -1, -11, -11, -15, -1, 3, -12, 7, 14, 1, 6, -6, 7, 1, 1, 0,
                -4, 8, 7, 2, 1, -2, -6, -14, -9, -3, -1, -12, -2, 7, 11, 4, 12, -14, -4, -4, 4, -1, 10, 3, -14, 1, 12,
                0, 10, -9, 8, -9, 14, -8, 8, 0, -3, 10, -6, 4, -8, 0, -1, -3, -8, -4, 8, 11, -3, -11, -8, 8, 3, 10, -3,
                -4, -4, -14, 12, 13, -8, -3, 12, -8, 4, 5, -1, -14, -8, 8, -3, -9, -15, 12, -5, -7, -15, -12, 11, -11,
                14, 11, 12, 3, 6, -6]

        self.assertEqual([
            [-4, -2, 6], [-4, 0, 4], [-4, 1, 3], [-4, 2, 2], [-2, -2, 4], [-2, 0, 2]
        ], sol.threeSum(nums))


        nums =[-15,13,6,-11,-4,5,-13,5,3,2,6,-1,4,12,-10,-13,-7,-4,-5,6,9,-14,1,-6,13,7,-8,10,-4,11,-8,-3,1,5,-7,4,-13,-13,-5,-3,4,-14,11,-14,5,-13,-12,13,-10,-10,-4,-15,13,13,-14,11,-3,-15,6,1,3,5,13,-11,-5,-9,1,-2,-14,11,10,5,4,-1,6,-6,-7,9,-15,-2,7,-12,-10,5,-14,13,-6,-9,6,7,7,-6,-2,-3,-9,0,-5,7,5,-4,-5,-7,-13,14,7,8,-15,7,-5,-15,-10,9]

        self.assertEqual([
            [-4, -2, 6], [-4, 0, 4], [-4, 1, 3], [-4, 2, 2], [-2, -2, 4], [-2, 0, 2]
        ], sol.threeSum(nums))


if __name__ == '__main__':
    unittest.main()
