# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print

tmp_stk = []
output_stk = []

def reset_stks():
    global tmp_stk, output_stk
    tmp_stk= []
    output_stk = []

def infix_to_prefix(str):
    reset_stks()
    for i in str:
        if ')' == i:
            clear_stk()
        elif i in ['(', '+', '-', '*', '/']:
            push_to_stk(i)
        else:
            output_stk.append(i)
    clear_stk()
    return "".join(output_stk)


def push_to_stk(item):
    tmp_stk.append(item)


def clear_stk():
    while True:
        if len(tmp_stk) == 0: break
        i = tmp_stk[-1]
        del tmp_stk[-1]
        if i == "(":
            break
        else:
            output_stk.append(i)


class TestCase(unittest.TestCase):
    def test_default(self):
        self.assertEqual(infix_to_prefix("(a+b)*(c+d)"), "ab+cd+*")
        self.assertEqual(infix_to_prefix("a+b*c"), "abc*+")


if __name__ == "__main__":
    unittest.main()
