import unittest
import pdb


class Solution(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        if not nums:
            return -1
        if len(nums) == 1:
            return 0
        if len(nums) == 2:
            return nums.index(max(nums[0], nums[1]))
        l = 0
        r = len(nums) - 1
        while l < r:
            mid_pos = (l + r) / 2
            mid1_pos = mid_pos + 1
            if (r - l) == 1:
                return nums.index(max(nums[l], nums[r]))
            if nums[mid1_pos] > nums[mid_pos]:
                l = mid_pos + 1
            else:
                r = mid_pos
        return l

class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(False, True)

# if __name__ == '__main__':
#    unittest.main()
