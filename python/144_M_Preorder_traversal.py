import unittest
import pdb


# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        res = []
        if not root:
            return []
        curr = root
        while curr:
            if not curr.left:
                curr = curr.right
            else:
                pre = curr.left
                while pre.right and pre.right != curr:
                    pre = pre.right
                if not pre.right:
                    res.append(curr.val)
                    pre.right = curr
                    curr = curr.left
                else:
                    curr = curr.right
                    pre.right = None
        return res


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(False, True)

# if __name__ == '__main__':
#    unittest.main()
