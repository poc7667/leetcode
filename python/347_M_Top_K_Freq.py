import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def update(self, child):
        parent = (child / 2)
        if parent <= 0:
            return None
        if self.outputs[child][1] > self.outputs[parent][1]:
            self.outputs[parent], self.outputs[child] = self.outputs[child], self.outputs[parent]
            self.update(parent)

    def build(self, freq):
        for k in freq:
            val = freq[k]
            self.outputs.append((k, val))
            last_index = len(self.outputs) - 1
            self.update(last_index)
            # print "build"
            # print(self.outputs)

    def get_children_max(self, i):
        last_index = len(self.outputs) - 1
        if i * 2 + 1 > last_index:
            return i * 2
        elif self.outputs[i * 2 + 1][1] > self.outputs[i * 2][1]:
            return i * 2 + 1
        else:
            return i * 2

    def update_after_removing(self, i):
        last_index = len(self.outputs) - 1
        if (i * 2) > last_index:
            return None
        else:
            child = self.get_children_max(i)
            if self.outputs[child][1] > self.outputs[i][1]:
                self.outputs[child], self.outputs[i] = self.outputs[i], self.outputs[child]
                self.update_after_removing(child)

    def remove(self):
        res = self.outputs[1][0]
        self.outputs[1] = self.outputs[-1]
        del self.outputs[-1]
        self.update_after_removing(1)
        # print "remove"
        # print(self.outputs)
        return res

    def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        freq = {}
        self.outputs = [None]
        for i in nums:
            if i in freq:
                freq[i] += 1
            else:
                freq[i] = 1
            # print i
            # print(freq)
        self.build(freq)
        # print self.outputs

        ans = []
        for i in range(k):
            ans.append(self.remove())
        # print self.outputs
        return ans


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        # self.assertEqual([1, 2], sol.topKFrequent([1, 1, 1, 2, 2, 3], 2))
        self.assertEqual([-1, 2], sol.topKFrequent([4, 1, -1, 2, -1, 2, 3], 2))


if __name__ == '__main__':
    unittest.main()
