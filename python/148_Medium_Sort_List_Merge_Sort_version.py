# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
import linked_list_util as t

p = print
import sys


# sys.setrecursionlimit(55)
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


def get_mid_node(start, termination_node=None):
    fast = start
    slow = start
    while fast != termination_node:
        fast = fast.next
        if fast == termination_node:
            break
        if fast.next != termination_node:
            slow = slow.next
            fast = fast.next
        else:
            break
    return slow

class Solution(object):
    def sortList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        # 只要有swap head 就要 update
        if head == None:
            return None
        else:
            self.head = head
            new_head = self.sort(head)
            return new_head

    def sort(self, head):
        if head.next != None:
            _left_head, _right_head = self.split_parts(head)
            left_list = self.sort(_left_head)
            right_list = self.sort(_right_head)
            new_head = self.merge(left_list, right_list)
            return new_head
        else:
            return head

    def split_parts(self, head):
        mid = get_mid_node(head)
        right_head = mid.next
        mid.next = None
        return head, right_head

    def merge(self, left_list, right_list):
        dummy = ListNode(-1)
        curr = dummy
        while left_list != None and right_list != None:
            if left_list.val < right_list.val:
                curr.next = left_list
                left_list = left_list.next
            else:
                curr.next = right_list
                right_list = right_list.next
            curr = curr.next
            curr

        if left_list == None:
            curr.next = right_list
        elif right_list == None:
            curr.next = left_list
        return dummy.next



def create_nodes(nums):
    nodes = []
    for i in nums:
        nodes.append(t.ListNode(i))
    for i in range(len(nodes) - 1):
        nodes[i].next = nodes[i + 1]
    return nodes[0]


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()

        self.assertEqual(app.sortList(None), None)

        head = create_nodes([4])
        self.assertEqual(t.ls(app.sortList(head)), [4])

        head = create_nodes([4, 3])
        self.assertEqual(t.ls(app.sortList(head)), [3, 4])

        head = create_nodes([2, 10, 3])
        self.assertEqual(t.ls(app.sortList(head)), [2, 3, 10])

        head = create_nodes([2, 10, 3, 7])
        self.assertEqual(t.ls(app.sortList(head)), [2, 3, 7, 10])

        head = create_nodes([2, 10, 7, 5, 3])
        self.assertEqual(t.ls(app.sortList(head)), [2, 3, 5, 7, 10])

        head = create_nodes([2, 10, 3, 7, 5])
        self.assertEqual(t.ls(app.sortList(head)), [2, 3, 5, 7, 10])

        head = create_nodes([2, 10, 3, 7, 5, 8])
        self.assertEqual(t.ls(app.sortList(head)), [2, 3, 5, 7, 8, 10])


        # mid_node = get_mid_node(head, head.next.next.next)
        # t1 = t.swap_nodes(head, head, head.next)


if __name__ == "__main__":
    unittest.main()
