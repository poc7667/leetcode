# -*- coding: utf8 -*-
# 34. Search for a Range
# Given a sorted array of integers, find the starting and ending position of a given target value.
#
# Your algorithm's runtime complexity must be in the order of O(log n).
#
# If the target is not found in the array, return [-1, -1].
#
# For example,
# Given [5, 7, 7, 8, 8, 10] and target value 8,
# return [3, 4].

# REFRENCE
# http://stackoverflow.com/questions/10148849/find-lowest-index-of-a-given-value-in-a-presorted-array
# https://www.youtube.com/watch?v=OE7wUUpJw6I

import unittest
import pdb

NO_RESULT = [-1, -1]
NO_FOUND = -1

class Solution(object):
    def searchRange(self, nums, t):

        if len(nums) == 0:
            return NO_RESULT
        if len(nums) == 1:
            return self.isThisOnlyOneElementQualified(nums, t)

        return [self.search_left_bound(nums, t), self.search_right_bound(nums, t)]

    def isThisOnlyOneElementQualified(self, nums, t):
        if nums[0] == t:
            return [0, 0]
        else:
            return NO_RESULT

    def search_right_bound(self, nums, t):
        print ("sub-array {nums} target: {t}".format(nums=nums, t=t))
        Lo = 0
        Hi = len(nums) - 1
        result = NO_FOUND
        while Lo <= Hi:
            mid = self.get_mid_cur(Lo, Hi)
            print("{Lo}/{Hi}/{Mid}".format(Lo=Lo, Hi=Hi, Mid=mid))
            if t == nums[mid]:
                result = mid
                Lo = mid + 1
            elif t < nums[mid]:
                Hi = mid - 1
            elif t > nums[mid]:
                Lo = mid + 1
        return result

    def search_left_bound(self, nums, t):
        Lo = 0
        Hi = len(nums) -1
        result = NO_FOUND
        while Lo <= Hi:
            mid = self.get_mid_cur(Lo, Hi)
            if t == nums[mid]:
                result = mid
                Hi = mid -1
            elif t > nums[mid]:
                Lo = mid +1
            elif t < nums[mid]:
                Hi = mid -1
        return  result


    def get_mid_cur(self, start, end):
        return start + (end - start) / 2


class MyTestCase(unittest.TestCase):
    def test_default_greeting_set(self):
        app = Solution()
        self.assertEqual(app.search_left_bound([1], 2), NO_FOUND)
        self.assertEqual(app.search_left_bound([1], 1), 0)
        self.assertEqual(app.search_right_bound([1,1,2,3,3], 3), 4)
        self.assertEqual(app.search_right_bound([1,1,2,3,4], 3), 3)
        self.assertEqual(app.search_left_bound([1, 1, 1, 2, 2, 3], 2), 3)
        self.assertEqual(app.search_left_bound([0,0, 1, 1, 1, 2, 2, 2, 3], 2), 5)
        self.assertEqual(app.search_left_bound([0,0, 1, 1, 1, 2, 2, 2, 3], 9), NO_FOUND)
        self.assertEqual(app.searchRange([1],1), [0,0])
        self.assertEqual(app.searchRange([1, 1, 2], 1), [0, 1])
        self.assertEqual(app.searchRange([2,2],2), [0,1])
        self.assertEqual(app.searchRange([2,2],-1), NO_RESULT)
        self.assertEqual(app.searchRange([1,4],4), [1,1])
        self.assertEqual(app.searchRange([5, 7, 7, 7, 7, 8, 8, 8, 10], 7), [1,4])
        self.assertEqual(app.searchRange([5, 7, 7, 8, 8, 10], 8), [3, 4])

if __name__ == '__main__':
    unittest.main()
