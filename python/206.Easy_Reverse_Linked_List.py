# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
import time
import tree_util
import tree_helper

p = print


# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.val:
            return str(self.val)
        else:
            return ""


class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        prev = None
        if head is None:
            return head
        while head:
            next_node = head.next
            head.next = prev
            prev = head
            head = next_node

        return prev

class TestCase(unittest.TestCase):
    def setUp(self):
        self.sol = Solution().reverseList

    def test_0(self):
        head = ListNode(1)
        self.assertEqual(self.sol(head).val, 1)

    def test_1(self):
        sol = Solution()
        head = ListNode(1)
        head.next = ListNode(2)
        self.assertEqual(sol.reverseList(head).val, 2)

if __name__ == "__main__":
    unittest.main()