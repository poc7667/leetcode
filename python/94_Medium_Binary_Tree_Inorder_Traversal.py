# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

# 1
#     2
#   3

# 1 3 2

p = print

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

# class Solution:
    # # @param root, a tree node
    # # @return a list of integers
    # def inorderTraversal(self, root):
    #     self.results = []
    #     self.doTraverse(root)
    #     return self.results
    #
    # def doTraverse(self, root):
    #     if root == None:
    #         return
    #     self.doTraverse(root.left)
    #     self.results.append(root.val)
    #     self.doTraverse(root.right)

    # Morris

    # Definition for a binary tree node.
    # class TreeNode(object):
    #     def __init__(self, x):
    #         self.val = x
    #         self.left = None
    #         self.right = None

class Solution(object):
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        outputs = []
        if not root:
            return outputs
        cur = root
        while cur:
            if cur.left == None:
                outputs.append(cur.val)
                cur = cur.right
            else:
                pred = cur.left
                while pred.right != cur and pred.right != None:
                    pred = pred.right
                if pred.right == None:
                    pred.right = cur
                    cur = cur.left
                elif pred.right == cur:
                    outputs.append(cur.val)
                    pred.right = None
                    cur = cur.right
        return outputs


class TestCase(unittest.TestCase):
    def test_default(self):
        s = Solution()
        root = TreeNode(1)
        root.right = TreeNode(2)
        root.right.left = TreeNode(3)
        self.assertEqual(s.inorderTraversal(root), [1, 3, 2])

if __name__ == "__main__":
    unittest.main()