# There are two sorted arrays nums1 and nums2 of size m and n respectively.
#
# Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
#
# Example 1:
# nums1 = [1, 3]
# nums2 = [2]
#
# The median is 2.0
# Example 2:
# nums1 = [1, 2]
# nums2 = [3, 4]
#
# The median is (2 + 3)/2 = 2.5

import unittest

import pdb


class Solution(object):
    def findMedianSortedArrays(self, a, b):
        self.median = median = (1+ (len(a) + len(b))) / 2 # pitfall
        if self.medianHasTwoNums(a, b):
            return (self.findKth(a, b, median) + self.findKth(a, b, median + 1)) / 2.0
        else:
            return self.findKth(a, b, median)

    def medianHasTwoNums(self, a, b):
        tot_len = len(a) + len(b)
        if (tot_len % 2) == 0:
            return True
        else:
            return False

    def findKth(self, a, b, k):
        """
        :type a: List[int]
        :type b: List[int]
        :type k: List[int]
        :rtype: float
        """
        if len(a) == 0:
            return b[k - 1]
        elif len(b) == 0:
            return a[k - 1]
        if 1 == k:
            return min(a[0], b[0])

        step = k / 2
        a_val = self.getVal(a, step)
        b_val = self.getVal(b, step)
        if a_val is None:
            if (a[-1] <= b_val):
                return self.findKth([], b, k - len(a))
            else:
                return self.findKth(a, b[step:], k - step)
        elif b_val is None:
            if (b[-1] <= a_val):
                return self.findKth(a, [], k - len(b))
            else:
                return self.findKth(a[step:], b, k - step)
        elif a_val <= b_val:
            return self.findKth(a[step:], b, k - step)
        else:
            return self.findKth(a, b[step:], k - step)


    def getVal(self, nums, step):
        if step <= len(nums):
            return nums[step - 1]
        else:
            return None


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        self.assertEqual(app.findMedianSortedArrays([1], [2]), 1.5) # 1.5
        self.assertEqual(app.findMedianSortedArrays([1, 3], [2]), 2)
        self.assertEqual(app.findMedianSortedArrays([1,2], [3,4]), 2.5)
        self.assertEqual(app.findMedianSortedArrays([1, 2, 3], [5, 6, 7]), 4)
        self.assertEqual(app.findMedianSortedArrays([4], [1, 2, 3, 5, 6]), 3.5)


if __name__ == "__main__":
    unittest.main()
