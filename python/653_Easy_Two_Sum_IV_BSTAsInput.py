# Given a Binary Search Tree and a target number, return true if there exist two elements in the BST such that their sum is equal to the given target.

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.values = []

    def seq_search(self, nums, k):
        for i in nums:
            if i.val == k:
                return True
        return False


    def findTarget(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: bool
        """
        self.inorder(root)
        for i in range(0,len(self.values)):
            remaining = k - self.values[i].val
            if self.seq_search(self.values[i+1:], remaining):
                return True
        return False
    def inorder(self, node):
        if node:
            self.inorder(node.left)
            self.values.append(node)
            self.inorder(node.right)