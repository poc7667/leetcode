import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):

    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        if n == 0:
            return 1
        pow = abs(n)
        cost = {}
        cost[0] = 1
        cost[1] = x
        cost[2] = x*x
        cost[3] = x*x*x
        paths = {}

        while pow / 2 > 1:
            if pow % 2 == 1:
                paths[pow] = [pow / 2, pow / 2, 1]
            else:
                paths[pow] = [pow / 2, pow / 2]
            pow /= 2

        for i in sorted(paths.keys()):
            print i, paths[i]
            ans = 1
            for node in paths[i]:
                ans*= cost[node]
            cost[i] = ans

        if n < 0:
            return (1.0/cost[abs(n)])
        else:
            return cost[abs(n)]


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()

        # self.assertEqual(0.25000, sol.myPow(8.66731, 87))
        # self.assertEqual(0.25000, sol.myPow(8.66731, 4))
        # self.assertEqual(0.25000, sol.myPow(0.00001, 177))
        # self.assertEqual(0.25000, sol.myPow(0.00001, 2147483647))

        self.assertEqual(32, sol.myPow(2.00, 5))
        self.assertEqual(1024, sol.myPow(2.00, 10))
        self.assertEqual(2.1*2.1*2.1, sol.myPow(2.1, 3))
        # self.assertEqual(0.25000, sol.myPow(0.00001, 2147483647))


if __name__ == '__main__':
    unittest.main()
