# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
import time
import tree_util
import tree_helper
from linked_list_util import *

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


class Solution(object):
    def reverseKGroup(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        if k <= 1:
            return head

        orig = ListNode(None)
        orig.next = head
        pseudo_head = orig

        while self.hasNext(pseudo_head, k):
            pseudo_head = self.reverse_segment(pseudo_head, k)

        return orig.next

    def hasNext(self, curr, k):
        tmp_curr = curr.next
        for i in range(k):
            if tmp_curr == None:
                return False
            tmp_curr = tmp_curr.next

        return True

    def reverse_segment(self, pseudo_head, k):  # k =3
        last_node = origin_head = pseudo_head.next
        current_node = last_node.next
        for i in range(k - 1):
            next_node = current_node.next
            current_node.next = last_node
            last_node = current_node
            current_node = next_node

        pseudo_head.next = last_node
        origin_head.next = current_node

        return origin_head


class TestCase(unittest.TestCase):
    def setUp(self):
        self.sol = Solution()


        #
        # self.head = ListNode(1)
        # self.head.next = ListNode(2)

    def test1(self):
        head = self.sol.reverseKGroup(create_a_linked_list_from_list([1, 2, 3, 4, 5]), 2)
        expected_values = [2, 1, 4, 3, 5]
        self.assertEqual(expected_values, convert_linked_list_into_a_list(head))

    def test2(self):
        head = self.sol.reverseKGroup(create_a_linked_list_from_list([1, 2, 3, 4, 5]), 3)
        expected_values = [3, 2, 1, 4, 5]
        self.assertEqual(expected_values, convert_linked_list_into_a_list(head))

    def test3(self):
        head = self.sol.reverseKGroup(create_a_linked_list_from_list([1, 2, 3, 4, 5, 6]), 3)
        expected_values = [3, 2, 1, 6, 5, 4]
        self.assertEqual(expected_values, convert_linked_list_into_a_list(head))


if __name__ == "__main__":
    unittest.main()
