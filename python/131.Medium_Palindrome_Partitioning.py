import unittest
import pdb


class Solution:

    def partition(self, L):
        # for i in range(1, len(L)):
        all_palindromes = self.create_all_palindrome_map(L)
        sol_map = self.traversal_solutions(all_palindromes, L)
        sols = self.build_strings(sol_map, L)
        return sols

    def build_strings(self, map, L):
        strings = []
        for i in range(len(L)+2,1, -1):
            if map.has_key(i):
                for path in map[i]:
                    this_str = []
                    for idx in range(len(path)-1):
                        this_str.append(L[path[idx]:path[idx+1]])
                    strings.append(this_str)
        return strings

    def create_all_palindrome_map(self, L):
        all_palindromes = {}
        for i in range(len(L)):
            all_palindromes[i] = []
            for j in range(i+1, len(L)+1):
                if self.isPalindrome(L[i:j]):
                    all_palindromes[i].append(j-1)
        return all_palindromes

    def isPalindrome(self, L):
        if len(L)==1:
            return True
        check_counts = len(L)/2
        for i in range(check_counts):
            if L[i]!=L[-(i+1)]:
                return False
        return True

    def traversal_solutions(self, map, L):
        solutions = {}
        Q = [[0]]
        while len(Q):
            tmp_Q = []
            for i in Q:
                for next_node in map[i[-1]]:
                    new_path = i+[next_node+1]
                    if new_path[-1] == len(L):
                        if len(new_path) in solutions:
                            solutions[len(new_path)].append(new_path)
                        else:
                            solutions[len(new_path)] = [new_path]
                    else:
                        tmp_Q.append(new_path)
            Q = tmp_Q

        return solutions



class TestCase(unittest.TestCase):
    def setUp(self):
        self.sol = Solution()

    def test_palindrome(self):
        self.assertEqual(self.sol.isPalindrome("ab"), False)
    def test(self):
        self.assertEqual(self.sol.partition("aab"), [["a", "a", "b"],["aa", "b"]])

if __name__ == "__main__":
    unittest.main()