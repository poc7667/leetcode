# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):

    def minCostClimbingStairs(self, cost):
        """
        :type cost: List[int]
        :rtype: int
        """
        acc_costs = []
        for i in range(len(cost)):
            if i < 2:
                acc_costs.append(cost[i])
            else:
                acc_costs.append(min(acc_costs[i - 1], acc_costs[i - 2]) + cost[i])

        return min([acc_costs[len(cost)-1], acc_costs[len(cost) - 2]])


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, False)
