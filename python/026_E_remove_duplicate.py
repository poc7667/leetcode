import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        last_number = None
        curr_number = None
        i = 0

        while (i + 1) <= len(nums):
            curr_number = nums[i]
            if last_number == curr_number:
                del nums[i]
                continue
            last_number = curr_number
            i += 1

        return len(nums)


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(5, sol.removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4]))


if __name__ == '__main__':
    unittest.main()
