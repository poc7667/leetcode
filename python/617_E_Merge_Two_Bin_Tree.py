# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)

# 作法: 把所有的subtree or value 都加總到左邊的 tree.
# edge cases: 轉移樹枝時候 記得消除右邊的 branches, assign =None

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def doMerge(self, t1, t2):
        if t2 == None:
            return False

        t1.val += t2.val
        if t1.left == None and t2.left:
            t1.left = t2.left
            t2.left = None
        if t1.right == None and t2.right:
            t1.right = t2.right
            t2.right = None
        self.doMerge(t1.left, t2.left)
        self.doMerge(t1.right, t2.right)

    def mergeTrees(self, t1, t2):
        """
        :type t1: TreeNode
        :type t2: TreeNode
        :rtype: TreeNode
        """
        if t1==None and t2!=None:
            t1,t2 = t2,t1
        self.doMerge(t1, t2)
        return t1


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, False)
