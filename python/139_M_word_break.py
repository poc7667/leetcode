# coding=utf-8
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# Input: s = "applepenapple", wordDict = ["apple", "pen"]
# Output: true
# Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
#              Note that you are allowed to reuse a dictionary word.

# 思路

# 一个DP问题。定义possible[i] 为S字符串上[0,i]的子串是否可以被segmented by dictionary.
#
# 那么
#
# possible[i] = true      if  S[0,i]在dictionary里面
#
#                 = true      if   possible[k] == true 并且 S[k+1,i]在dictionary里面， 0<k<i
#
#                = false      if    no such k exist.

# class Solution(object):
#     def wordBreak(self, s, wordDict):
#         """
#         :type s: str
#         :type wordDict: List[str]
#         :rtype: bool
#         """
#         newDict = list()
#         for i in wordDict:
#             if i in s:
#                 newDict.append(i)
#         charsInS = set()
#         [charsInS.add(i) for i in s if i not in charsInS]
#         word_set = set(newDict)
#         if charsInS in word_set:
#             return True
#
#         remaining = s[:]
#         q = Queue.Queue()
#         q.put(remaining)
#         while not q.empty():
#             remaining = q.get()
#             for i in newDict:
#                 if remaining == i:
#                     return True
#                 if remaining.startswith(i):
#                     q.put(remaining[len(i):])
#         return False
class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        ans = {}
        print s
        for i in range(0, len(s))[::-1]:
            suffix = s[i:]
            print suffix
            if suffix in wordDict:
                ans[i] = True
                continue
            else:
                ans[i] = False
                for j in wordDict:
                    if suffix.startswith(j):
                        suffix_idx = len(j) + i
                        if suffix_idx in ans and ans[suffix_idx]:
                            ans[i] = True
        return ans[0]


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, sol.wordBreak('leetcode', ['leet', 'code']))
        self.assertEqual(True, sol.wordBreak('applebed', ['apple', 'bed']))
        self.assertEqual(True, sol.wordBreak('applepen', ['apple', 'pen']))
        self.assertEqual(True, sol.wordBreak('applepenapple', ['apple', 'pen']))


        # s = "acaaaaabbbdbcccdcdaadcdccacbcccabbbbcdaaaaaadb"
        # banks = ["abbcbda", "cbdaaa", "b", "dadaaad", "dccbbbc", "dccadd", "ccbdbc", "bbca", "bacbcdd", "a", "bacb",
        #          "cbc",
        #          "adc", "c", "cbdbcad", "cdbab", "db", "abbcdbd", "bcb", "bbdab", "aa", "bcadb", "bacbcb", "ca",
        #          "dbdabdb",
        #          "ccd", "acbb", "bdc", "acbccd", "d", "cccdcda", "dcbd", "cbccacd", "ac", "cca", "aaddc", "dccac",
        #          "ccdc",
        #          "bbbbcda", "ba", "adbcadb", "dca", "abd", "bdbb", "ddadbad", "badb", "ab", "aaaaa", "acba", "abbb"]
        #
        # self.assertEqual(True, sol.wordBreak(s, banks))
        # #
        # s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
        # banks = ["a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaa", "aaaaaaaaaa"]
        # self.assertEqual(True, sol.wordBreak(s, banks))


if __name__ == '__main__':
    unittest.main()
