# -*- coding: utf8 -*-



# Given a binary tree, return all root-to-leaf paths.
#
# For example, given the following binary tree:
#
#    1
#  /   \
# 2     3
#  \
#   5
# All root-to-leaf paths are:
#
# ["1->2->5", "1->3"]

from __future__ import print_function

import pdb
import unittest
import tree_util
p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


class Solution:
    # @param {TreeNode} root
    # @return {string[]}
    def binaryTreePaths(self, root):
        if root == None: return []
        Q = [root]
        non_leafs = set()
        ans = []
        while len(Q) > 0:
            u = Q.pop()
            if u.left:
                non_leafs.add(u)
                Q.append(u)
                Q.append(u.left)
                u.left = None
            elif u.right:
                non_leafs.add(u)
                Q.append(u)
                Q.append(u.right)
                u.right = None
            else:
                if u not in non_leafs:
                    ans.append("->".join(self.toString(Q + [u])))
        return ans
    def toString(self, nodes):
        return [ str(i.val) for i in nodes]

class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = Solution()
        self.root = tree_util.create_tree([3, 9, 20, None, None, 7, 15])

    def test_default(self):
        self.assertEqual(self.app.binaryTreePaths(self.root),
                         set(["3->9",
                              "3->20->15",
                              "3->20->7"
                              ]))


if __name__ == "__main__":
    unittest.main()