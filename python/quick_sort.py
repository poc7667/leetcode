# -*- coding: utf8 -*-

import pdb
import unittest


def qs(nums):
    # termination
    if len(nums) == 1:
        return [nums[0]]
    if len(nums) == 2:
        if nums[0] > nums[1]:
            swap(nums)
        return nums

    wall = 0
    for i in range(0, len(nums) - 1):
        if nums[i] < nums[-1]:
            swap(nums, wall, i)
            wall += 1

    swap(nums, wall, -1)

    if wall==0:
        return [nums[0]] + nums[1:-1]
    elif wall==len(nums)-1:
        return nums[0:-2] + [nums[-1]]
    else:
        return nums[0:wall]+ nums[wall:wall+1]+ nums[wall+1:len(nums)]

def swap(nums, i, j):
    temp = nums[i]
    nums[i] = nums[j]
    nums[j] = temp



# 2018-08-19
def qs(nums):
    # Termination cond
    if len(nums) == 1:
        return [nums[0]]

    if len(nums) == 2:
        if nums[0] > nums[1]:
            swap(nums, 0, 1)
        return nums

    pivot_value = nums[len(nums) - 1]
    wall = 0
    for curr in range(0, len(nums) - 1):
        if nums[curr] < pivot_value:
            swap(nums, curr, wall)
            wall += 1

    swap(nums, wall, len(nums) - 1)

    if wall == 0:
        return nums[0:1] + qs(nums[wall + 1: len(nums)])
    elif wall == len(nums) - 1:
        return qs(nums[0:len(nums) - 1]) + [nums[-1]]
    else:
        return qs(nums[0:wall]) + nums[wall: wall + 1] + qs(nums[wall + 1:len(nums)])


def swap(nums, i, j):
    temp = nums[i]
    nums[i] = nums[j]
    nums[j] = temp
    return nums


# def qs(nums):
#     sort_helper(nums, 0, len(nums)-1)
#     return nums
#
# def sort_helper(nums, l, r):
#     if l<r:
#         left_bound = l
#         for i in range(l, r):
#             if nums[i] < nums[r]:
#                 nums[i], nums[left_bound] = nums[left_bound], nums[i]
#                 left_bound+=1
#         nums[r], nums[left_bound] = nums[left_bound], nums[r]
#         sort_helper(nums, l, left_bound-1)
#         sort_helper(nums, left_bound+1, r)

class TestCase(unittest.TestCase):
    def test_default(self):
        self.assertEqual(qs([3, 7, 1, 5]), [1, 3, 5, 7])
        self.assertEqual([-2321, 0, 7, 222], qs([7, -2321, 222, 0]))
        self.assertEqual([-2321, 0, 6, 7, 8, 9, 10, 222, 5245], qs([7, 6, 9, 10, 8, -2321, 5245, 222, 0]))
        self.assertEqual([6, 7, 8, 9, 10], qs([7, 6, 9, 10, 8]))
        self.assertEqual(
            [1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 8, 9, 9, 9],
            qs([3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3])
        )
        self.assertEqual([3],qs([3]))
        self.assertEqual( [1, 3, 5], qs([3, 1, 5]))
        self.assertEqual( [3, 5], qs([5, 3]))


if __name__ == "__main__":
    unittest.main()
