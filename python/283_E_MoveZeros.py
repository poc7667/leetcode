# -*- coding: UTF-8 -*-
import unittest
import pprint
import pdb
import time
import Queue

pp = pprint.PrettyPrinter(indent=4)


# Input: [0,1,0,3,12]
# Output: [1,3,12,0,0]


class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        for i in range(len(nums) - 1)[::-1]:
            if nums[i] == 0:
                j = i
                while j + 1 < len(nums) and nums[j + 1] != 0:
                    nums[j], nums[j + 1] = nums[j + 1], nums[j]
                    j += 1


class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(True, False)
