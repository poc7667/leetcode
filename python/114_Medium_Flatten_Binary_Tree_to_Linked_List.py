# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
# AC Rate: 28.1%
# SOURCE URL:
# https://oj.leetcode.com/problems/flatten-binary-tree-to-linked-list/
#
#
# Given a binary tree, flatten it to a linked list in-place.
#
#
# For example,
# Given
#
#          1
#         / \
#        2   5
#       / \   \
#      3   4   6
#
#
#
# The flattened tree should look like:
#
#    1
#     \
#      2
#       \
#        3
#         \
#          4
#           \
#            5
#             \
#              6
#
# click to show hints.
# Hints:
# If you notice carefully in the flattened tree, each node's right child
# points to the next node of a pre-order traversal.
#
#
import unittest

p = print

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")

# Definition for a  binary tree node
class TreeNode:

    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:

    def flatten(self, root):
        self.head = ListNode(-1)
        self.curr = self.head
        self.traverse(root)
        return self.head.next

    def traverse(self, root):
        if root!=None:
            self.curr.next = ListNode(root.val)
            self.curr = self.curr.next
            self.traverse(root.left)
            self.traverse(root.right)

class TestCase(unittest.TestCase):
    
    def build_tree(self):
        root = TreeNode(1)
        root.left = TreeNode(2)
        root.left.left = TreeNode(3)
        root.left.right = TreeNode(4)
        root.right = TreeNode(5)
        root.right.right = TreeNode(6)
        return root


    def test_default(self):
        app = Solution()
        root = self.build_tree()
        rtn_node = app.flatten(root)


if __name__ == "__main__":
    unittest.main()