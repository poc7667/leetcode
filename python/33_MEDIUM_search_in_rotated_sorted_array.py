# 33. Search in Rotated Sorted Array  QuestionEditorial Solution  My Submissions
# Total Accepted: 118838
# Total Submissions: 381406
# Difficulty: Hard
# Suppose a sorted array is rotated at some pivot unknown to you beforehand.
#
# (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
#
# You are given a target value to search. If found in the array return its index, otherwise return -1.
#
# You may assume no duplicate exists in the array.

import unittest
import pdb

NOT_FOUND = -1
class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if nums == None or len(nums)==0:
            return NOT_FOUND
        if len(nums) == 1:
            if target == nums[0]:
                return 0
            else:
                return NOT_FOUND
        if len(nums) == 2:
            if target == nums[0]:
                return 0
            elif target == nums[1]:
                return 1
            else:
                return NOT_FOUND
        l = 0
        r = len(nums) - 1
        mid = (l + r) / 2
        while l + 1 < r:
            mid = (l + r) / 2
            if nums[mid] == target:
                return mid
            elif nums[l] == target:
                return l
            elif nums[r] == target:
                return r

            if nums[l] < nums[mid]:
                if nums[l] < target < nums[mid]:
                    r = mid - 1
                else:
                    l = mid + 1
            else:
                if nums[mid] < target < nums[r]:
                    l = mid + 1
                else:
                    r = mid - 1
        if nums[mid] == target:
            return mid
        elif nums[l] == target:
            return l
        elif nums[r] == target:
            return r

        return NOT_FOUND


class TestCase(unittest.TestCase):
    def test_default(self):
        app = Solution()
        self.assertEqual(app.search([3, 4, 5, 1, 2], 4), 1)
        self.assertEqual(app.search([3], 4), NOT_FOUND)
        self.assertEqual(app.search([3], 3), 0)
        self.assertEqual(app.search([3, 5], 4), NOT_FOUND)
        self.assertEqual(app.search([3, 5], 5), 1)
        self.assertEqual(app.search([3, 5], 3), 0)
        self.assertEqual(app.search([3, 1], 3), 0)
        self.assertEqual(app.search([3, 1], 1), 1)
        self.assertEqual(app.search([4, 5, 6, 7, 0, 1, 2], 4), 0)


# if __name__ == '__main__':
#     unittest.main()
