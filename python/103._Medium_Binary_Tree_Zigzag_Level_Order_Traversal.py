# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest
import time
import tree_util
import tree_helper

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")


class Solution(object):
    def zigzagLevelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root:
            return []
        Q = [root]
        output = []
        should_be_reversed = True
        while Q:
            next_level = []
            for i in range(len(Q)):
                if Q[i].left:
                    next_level.append(Q[i].left)
                if Q[i].right:
                    next_level.append(Q[i].right)
            should_be_reversed = not should_be_reversed
            self.push_output(should_be_reversed, Q, output)
            Q = [i for i in next_level]
        return output

    def push_output(self, reversed_direction, Q, output):
        if reversed_direction:
            output.append([i.val for i in Q[::-1]])
        else:
            output.append([i.val for i in Q])

class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = Solution()
        self.tree = tree_helper.Tree()
        self.fixture = [3, 9, 20, None, None, 15, 7]
        self.expected_output = [
            [3],
            [20, 9],
            [15, 7]
        ]
        self.tree.load_array(self.fixture)

    def test_default(self):
        self.assertEqual(self.app.zigzagLevelOrder(self.tree.head), self.expected_output)


if __name__ == "__main__":
    unittest.main()
