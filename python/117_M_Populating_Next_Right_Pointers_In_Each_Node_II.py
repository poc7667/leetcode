import unittest
import pdb
# Definition for binary tree with next pointer.
# class TreeLinkNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#         self.next = None

class Solution:
    # @param root, a tree link node
    # @return nothing

    # 思路:
    # 用三個指標來控制 looping all the levels
    # Parent: 用來控制左右
    def connect(self, root):
        if root:
            curr = root
            child_head = None
            child = None
            while curr:
                while curr:
                    if curr.left:
                        if not child_head:
                            child_head = curr.left
                            child = child_head
                        else:
                            child.next = curr.left
                            child = child.next
                    if curr.right:
                        if not child_head:
                            child_head = curr.right
                            child = child_head
                        else:
                            child.next = curr.right
                            child = child.next
                    curr = curr.next
                if child_head:
                    curr = child_head
                child_head = None
                child = None

class MyTestCase(unittest.TestCase):
    def test_something(self):
        sol = Solution()
        self.assertEqual(False, True)

# if __name__ == '__main__':
#    unittest.main()
