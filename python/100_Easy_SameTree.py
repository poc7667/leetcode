# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isSameTree(self, p, q):
        if p == None and q == None:
            return True
        else:
            return self.compare_val(p, q) and \
                   self.isSameTree(p.left, q.left) and \
                   self.isSameTree(p.right, q.right)

    def compare_val(self, p, q):
        if p != None and q != None:
            return p.val == q.val
        else:
            return False

            # if p==None and q==None:
            #     return
            # if p.val != q.val:
            #     return False
            # return self.isSameTree(p.left, q.right)


class TestCase(unittest.TestCase):
    def test_default(self):
        self.assertEqual()


if __name__ == "__main__":
    unittest.main()
