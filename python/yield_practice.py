# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


# Definition for a binary tree node.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def __repr__(self):
        if self.next != None:
            return "{value} ({next})".format(value=self.val, next=self.next.val)
        else:
            return "{value} ({next})".format(value=self.val, next="nil")

import datetime
def test_yield():
    for i in range(4):
        t_time =  (datetime.datetime.now().time())
        yield t_time

for t in test_yield():
    print(t)
