graph = {'A': set(['B', 'C']),
         'B': set(['A', 'D', 'E']),
         'C': set(['A', 'F']),
         'D': set(['B']),
         'E': set(['B', 'F']),
         'F': set(['C', 'E'])}


def dfs_paths(graph, start, goal):
    stack = [(start, [start])]
    while stack:
        (vertex, path) = stack.pop()
        print(vertex, path)
        print("For", graph[vertex], set(path) ,graph[vertex] - set(path))
        for next in graph[vertex] - set(path):
            if next == goal:
                yield path + [next]
            else:
                stack.append((next, path + [next]))


for i in  list(dfs_paths(graph, 'A', 'F')): # [['A', 'C', 'F'], ['A', 'B', 'E', 'F']]
    print("ans", i)