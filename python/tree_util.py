# -*- coding: utf8 -*-
from __future__ import print_function

import pdb
import unittest

p = print


class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


    def __repr__(self):
        if self:
            return "{value} ".format(
                value=self.val,
            )
        else:
            return "{value}".format(value="nil")


def create_tree(nums):
    return create_node(nums, 0)


def create_node(nums, idx):
    if idx < len(nums) and nums[idx]!=None:
        root = TreeNode(nums[idx])
        root.left = create_node(nums, get_left_child_index(idx))
        root.right = create_node(nums, get_right_child_index(idx))
        return root


def find_by_value(root, v):
    if root:
        if root.val == v:
            return root
        else:
            if find_by_value(root.left, v):
                return find_by_value(root.left, v)
            elif find_by_value(root.right, v):
                return find_by_value(root.right, v)
    else:
        return False

def get_left_child_index(idx):
    return (idx + 1) * 2 - 1


def get_right_child_index(idx):
    return (idx + 1) * 2


class TestCase(unittest.TestCase):
    def test_default(self):
        root = create_tree([3, 9, 20, None, None, 15, 7])
        pdb.set_trace()


if __name__ == "__main__":
    unittest.main()
