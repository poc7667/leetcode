#include <iostream>

int SIZE = 3;

void printOutput(int *);

void init_array(int *a, int size) {
    for (int i = 0; i < size; i++) {
        a[i] = i+1;
    }
}

void perm(int *a, int start) {
    if (start == SIZE - 1) {
        printOutput(a);
    } else {

        for (int i = start; i < SIZE; i++) {
            int temp = a[i];
            a[i] = a[start];
            a[start] = temp;
            perm(a, start+1);
            temp = a[i];
            a[i] = a[start];
            a[start] = temp;

        }

    }

}

int main() {
    int a[SIZE];
    init_array(a, SIZE);
    perm(a, 0);
    return 0;
}

void printOutput(int *a) {
    for (int i = 0; i < SIZE; i++) {
        std::cout << a[i];
    }
    std::cout << "\n";
}