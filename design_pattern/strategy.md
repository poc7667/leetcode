

![inline](https://www.dropbox.com/s/ez5nf8d24ofc376/Screenshot%202017-05-07%2010.44.14.png?dl=0=300x "Title")

![inline](https://www.dropbox.com/s/1eted1lsl12jw32/Screenshot%202017-05-07%2010.44.38.png?dl=0=300x "Title")

      def show_change_password
        request.format = 'html'
        @email = params['email']
        @confirmation_token = params['confirmation_token']
        registration = EmailRegistration.where({email: params['email'], confirmation_token: params['confirmation_token']}).first
        user = User.where(email: params['email']).first
        respond_to do |format|
          if registration
            if user
              if registration.token == 'reset_password'
                format.html { render 'show_change_password' }
              else
                user.update_token(registration.token)
                LoginToken.create({email: user.email, token: registration.token})
                format.html { render plain: "#{user.email} verified" }
              end
            elsif registration.status == 'requested'
              format.html { render 'show_change_password' }
            else
              format.html { render plain: 'The password has already been reset' }
            end
          else
            format.html { render plain: 'Unknown exception' }
          end
        end
      end


