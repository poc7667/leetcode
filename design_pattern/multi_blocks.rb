class Validator
    attr_accessor :validators
    def initialize
        self.validators = []
    end

    def add_validator(cb, argument)
        self.validators << cb
    end
    
    def do
        self.validators.each do |block|
            puts block.class
            block.call(123)
        end
    end
end

def yoyo(data)
    puts "yoyo #{data}"
end

def block_arg_test(&block)
  block.call 1   # block[1] ¤Ç¤â¤è¤¤
  yield 2        # ÅöÁ³yield¤â»È¤¨¤ë
end
block_arg_test {|x| p x }

# def lala
#     puts 123
# end

vali = Validator.new
puts vali
vali.add_validator(method(:yoyo), 1234)
# vali.add_validator(lala)
vali.do
# validator(&yoyo, &lala)