# Video reference

- Bin Search (first and last occurrance)
- https://www.youtube.com/watch?v=OE7wUUpJw6Ic

### 題型整理

https://github.com/sevenhe716/LeetCode/tree/1dc695260f449c37f49977a40f2f68c77a29e46b

### Pitfall 
    0 == None == False in if-else condition

    if 0 // false
    if None // false

    Python 交換兩個數字
    
    Python中两个数交换可以用如下方法实现：

    a, b = b, a
    
    或者：
    a ^= b
    b ^= a
    a ^= b

# 考題整理

https://github.com/kamyu104/LeetCode


整理很好的筆記以及一些小技巧　

https://www.zybuluo.com/smilence/note/76

Singly linked list: 直接操作prev, prev->next 比較簡潔

針對　head node 存取，直接　new dummy node, return dummy->next 即為所求

link list 奇技淫巧
http://www.caifengyu.net/blog/2014/01/05/linkedlist/

数据结构与算法/leetcode/lintcode题解

http://www.kancloud.cn/kancloud/data-structure-and-algorithm-notes/72916
寫的很棒


swap node, 需要檢查　nodes 是否　adjacent.

singly link lisk 非常容易遇到　traverse INF loop,
再找尋兩點距離，需要確保　node, node 先後次序

start_head = self.swap_nodes(start_head, wall, pivot)
更新start_head 很重要，因為你的start_head 很可能在swap 過程中被替換調。
他不是list, 他會被換到你不知道的地方。

# 位元運算 bit operation

http://baike.baidu.com/view/379209.htm

利用two's complementary 表示 negative # http://tim.diary.tw/2009/08/24/twos-complement/

1補數(1'S)有號數的加／減法運算法則與前節介紹的正整數加／減法完全相同，也就是加法直接相加，減法有三個原則(1)被減數保持不變，減數取1補數後相加，(2)相加後若產生進位，表示其結果為正數，必須端迴進位，(3)相加後若無進位，表示其結果為負數，此結果為1的補數形式。

http://heycodergirl.blogspot.com/2015/10/leetcode-bit-manipulation.html

# Tree implementation

https://gist.github.com/thinkphp/1450738

Dummy node 非常非常非常重要

link 適時的把它切斷可以大幅度降低複雜度

https://github.com/Lancert9/DSAA

快慢potiner 找mid point的問題，記得要確認抓出來的mid 是 N/2, 而不是N/2 +1

# Python 中，手動限制 max # of recursions
    import sys
    sys.setrecursionlimit(num_vertices + 2)

# HEAP

https://www.cs.cmu.edu/~adamchik/15-121/lectures/Binary%20Heaps/heaps.html


# 偵測cycle 的方法

     https://www.ptt.cc/man/C_and_CPP/DDD2/D4A8/M.1265939639.A.AA3.html

     https://github.com/qiwsir/algorithm/blob/master/kruskal_algorithm.md

     師大資料結構網站
     http://www.csie.ntnu.edu.tw/~u91029/


# List 操作

range(l, m) : l~m-1 的整數

## chunck

    def chunks(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]


# Kruskal

    https://github.com/israelst/Algorithms-Book--Python

# 非常好的各式各樣演算法整理 利用 Python

    https://github.com/qiwsir/algorithm
    https://github.com/silky/PADS-mirror/tree/2c6871493bdf8755b966ac7d8f780d3e42958018


    # Yield
        yield from range(10) 这种简单语句， 可以等价于 for i in range(10): yield i 。

    # DFS 用iterative 方式求解

        http://www.cnblogs.com/zichi/p/4807752.html

    # 演算法影片

        https://www.youtube.com/playlist?list=PLqD_OdMOd_6b4qyFfJpputeXFWGX20f69